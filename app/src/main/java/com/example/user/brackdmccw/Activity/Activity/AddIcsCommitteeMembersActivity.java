package com.example.user.brackdmccw.Activity.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.NetworkCall.APIUtils.ApiUtils;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.AddMemberAPIService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.AddMemberPojo.AddMemberResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.LoginResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.CommitteeDesignation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.District;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.IcsUdmt;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.PersonalInformation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.Upazilla;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.UserLoginSuccessResponse;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.user.brackdmccw.R.id.etPin;

public class AddIcsCommitteeMembersActivity extends Activity {


    Button btn_back;
    Button btn_home;
    Button buttonHome;
    Button btnMenu;
    Button btn_login;
    Button btnAdd;
    Button btnCancel;

    EditText input_pin;
    EditText input_first_name;
    EditText input_last_name;
    EditText input_programme;
    EditText input_designation;
    EditText input_comittee_designation;
    Button input_date_of_join;
    EditText input_mobile_number;

    DatabaseHandler databaseHandler;

    String designationFromDB = "";
    String firstNameFromDB = "";
    String lastNameFromDB = "";
    String picNameFromDB = "";
    String programFromDB = "";
    String committeeDesignationFromDB = "";
    String mobileFromDB = "";
    String dojFromDB = "";
    String dobFromDB = "";
    String memberTypeIdFromDB = "";
    String committeeTypeIdFromDB = "";


    AddMemberAPIService addMemberAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add_ics_committee_members);

        addMemberAPIService = ApiUtils.getAddMemberAPIService();

//******************** Hide Actionbar**************

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //getSupportActionBar().setCustomView(R.layout.header);

//******************** Hide Actionbar**************

//*********************** View Initialize ********************

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_home = (Button) findViewById(R.id.btn_home);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        btn_login = (Button) findViewById(R.id.btn_login);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        input_pin = (EditText) findViewById(R.id.etPin);
        input_first_name = (EditText) findViewById(R.id.etFirstName);
        input_last_name = (EditText) findViewById(R.id.etLastName);
        input_programme = (EditText) findViewById(R.id.etProgramme);
        input_designation = (EditText) findViewById(R.id.etDesignation);
        input_comittee_designation = (EditText) findViewById(R.id.etCommitteeDesignation);
        input_date_of_join = (Button) findViewById(R.id.etDateOfJoin);
        input_mobile_number = (EditText) findViewById(R.id.etMobile);

        databaseHandler = new DatabaseHandler(this);
//***********************End View Initialize ********************


        //*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************


//**********************Back Button Click Event *********************

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

//********************** Button Click Event *********************


//********** When Enter Pin In EditText Data Parse From Db And Display The EditText **********
        input_pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                input_first_name.setText("First Name");
                input_last_name.setText("Last Name");
                input_programme.setText("Program");
                input_designation.setText("Designation");
                input_comittee_designation.setText("Committee Designation");
                input_date_of_join.setText("Doj");
                input_mobile_number.setText("Mobile");

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//********************** Get Information From DB By PIN *********************

                String pinQuerry = "SELECT  dmcc_personal_information.*," +
                        " dmcc_committee_designation.committee_designation," +
                        " dmcc_ics_udmt.committee_type_id," +
                        " dmcc_ics_udmt.member_type_id" +
                        " FROM dmcc_personal_information" +
                        " INNER JOIN dmcc_ics_udmt" +
                        " ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id" +
                        " INNER JOIN dmcc_committee_designation " +
                        " ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id" +
                        " WHERE pin = "+"'"+s.toString()+"'";//Write in 01-01-2018

                String pinDataQry = "SELECT * FROM dmcc_personal_information" +
                        " INNER JOIN dmcc_ics_udmt ON dmcc_personal_information.id = " +
                        " dmcc_ics_udmt.personal_information_id" +
                        " INNER JOIN dmcc_designation ON dmcc_designation.id = " +
                        " dmcc_personal_information.designation_id" +
                        " WHERE pin = "+"'"+s.toString()+"'";

                try{
                    Cursor pinDataCursor = databaseHandler.rawQuery(pinQuerry);
                    if (pinDataCursor != null){
                        if (pinDataCursor.moveToFirst()){
                            do{

                                firstNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("first_name"));
                                input_first_name.setText(firstNameFromDB);

                                lastNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("last_name"));
                                input_last_name.setText(lastNameFromDB);

                                designationFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("company_designation"));
                                input_designation.setText(designationFromDB);

                                dobFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("dob"));

                                dojFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("doj"));
                                input_date_of_join.setText(dojFromDB);

                                picNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("pic_name"));

                                programFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("program_name"));
                                input_programme.setText(programFromDB);

                                committeeDesignationFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("committee_designation"));
                                input_comittee_designation.setText(committeeDesignationFromDB);

                                mobileFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("mobile"));
                                input_mobile_number.setText(mobileFromDB);

                                memberTypeIdFromDB = String.valueOf(pinDataCursor.getInt(pinDataCursor.getColumnIndex("member_type_id")));
                                committeeTypeIdFromDB = String.valueOf(pinDataCursor.getInt(pinDataCursor.getColumnIndex("committee_type_id")));

                            }while (pinDataCursor.moveToNext());
                        }
                    }
                }catch (Exception ex){}


//**********************End Get Information From DB By PIN *********************
            }

            @Override
            public void afterTextChanged(Editable s) {

//********************** Get Information From DB By PIN *********************

                String pinQuerry = "SELECT  dmcc_personal_information.*," +
                        " dmcc_committee_designation.committee_designation," +
                        " dmcc_ics_udmt.committee_type_id," +
                        " dmcc_ics_udmt.member_type_id" +
                        " FROM dmcc_personal_information" +
                        " INNER JOIN dmcc_ics_udmt" +
                        " ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id" +
                        " INNER JOIN dmcc_committee_designation " +
                        " ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id" +
                        " WHERE pin = "+"'"+s.toString()+"'";//Write in 1-1-2018


                String pinDataQry = "SELECT * FROM dmcc_personal_information" +
                        " INNER JOIN dmcc_ics_udmt ON dmcc_personal_information.id = " +
                        " dmcc_ics_udmt.personal_information_id" +
                        " INNER JOIN dmcc_designation ON dmcc_designation.id = " +
                        " dmcc_personal_information.designation_id" +
                        " WHERE pin = "+"'"+s.toString()+"'";

                try {
                    Cursor pinDataCursor = databaseHandler.rawQuery(pinQuerry);
                    if (pinDataCursor != null) {
                        if (pinDataCursor.moveToFirst()) {
                            do {

                                firstNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("first_name"));
                                input_first_name.setText(firstNameFromDB);

                                lastNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("last_name"));
                                input_last_name.setText(lastNameFromDB);

                                designationFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("company_designation"));
                                input_designation.setText(designationFromDB);

                                dobFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("dob"));

                                dojFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("doj"));
                                input_date_of_join.setText(dojFromDB);

                                picNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("pic_name"));

                                programFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("program_name"));
                                input_programme.setText(programFromDB);

                                committeeDesignationFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("committee_designation"));
                                input_comittee_designation.setText(committeeDesignationFromDB);

                                mobileFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex("mobile"));
                                input_mobile_number.setText(mobileFromDB);

                                memberTypeIdFromDB = String.valueOf(pinDataCursor.getInt(pinDataCursor.getColumnIndex("member_type_id")));
                                committeeTypeIdFromDB = String.valueOf(pinDataCursor.getInt(pinDataCursor.getColumnIndex("committee_type_id")));

                            } while (pinDataCursor.moveToNext());
                        }
                    }
                }catch (Exception ex){}

//**********************End Get Information From DB By PIN *********************

            }//End afterTextChanged Method

        });//End addTextChangedListener

//****** When Enter Pin In EditText Data Parse From Db And Display The EditText **********


//**********************Add  Button Click Event *********************

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        String pin = input_pin.getText().toString().trim();
                        String first_name = input_first_name.getText().toString().trim();
                        String last_name = input_last_name.getText().toString().trim();
                        String program = input_programme.getText().toString().trim();
                        String company_designation = input_designation.getText().toString().trim();
                        String committee_designation = input_comittee_designation.getText().toString().trim();
                        String doj = input_date_of_join.getText().toString().trim();
                        String mobile = input_mobile_number.getText().toString().trim();

                        if(!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(first_name)&& !TextUtils.isEmpty(last_name)&& !TextUtils.isEmpty(program)&& !TextUtils.isEmpty(company_designation)&& !TextUtils.isEmpty(committee_designation)&& !TextUtils.isEmpty(doj)&& !TextUtils.isEmpty(mobile)) {

                            if(!first_name.equalsIgnoreCase("First Name")
                                &&!last_name.equalsIgnoreCase("Last Name")
                                &&!program.equalsIgnoreCase("Program")
                                &&!company_designation.equalsIgnoreCase("Designation")
                                &&!committee_designation.equalsIgnoreCase("Committee Designation")
                                &&!doj.equalsIgnoreCase("Doj")&&!mobile.equalsIgnoreCase("Mobile")){

                                if(isNetworkConnected()){
                                    addCommitteeMember(pin,first_name,last_name,program,
                                            company_designation,
                                            committee_designation,doj,mobile);
                                    Intent hsintent = new Intent(AddIcsCommitteeMembersActivity.this,ListOfIcsCommitteeMembersAddNewActivity.class);
                                    startActivity(hsintent);
                                    finish();

                                }else{

                                    Toast.makeText(AddIcsCommitteeMembersActivity.this, "Please Check Your Network Connection",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }


                        }else if(TextUtils.isEmpty(pin)){
                            input_pin.setError("Please enter pin");
                        }else if(TextUtils.isEmpty(first_name)){
                            input_first_name.setError("Please enter first name");
                        }else if(TextUtils.isEmpty(last_name)){
                            input_first_name.setError("Please enter last name");
                        }else if(TextUtils.isEmpty(program)){
                            input_programme.setError("Please enter program");
                        }else if(TextUtils.isEmpty(company_designation)){
                            input_designation.setError("Please enter designation");
                        }else if(TextUtils.isEmpty(committee_designation)){
                            input_comittee_designation.setError("Please enter committee " +
                                    "designation");
                        }else if(TextUtils.isEmpty(doj)){
                            input_date_of_join.setError("Please enter doj");
                        }else if(TextUtils.isEmpty(mobile)){
                            input_mobile_number.setError("Please enter mobile");
                        }
                        else{
                            input_pin.setError("Please enter pin");
                            input_first_name.setError("Please enter first name");
                            input_last_name.setError("Please enter last name");
                            input_programme.setError("Please enter program");
                            input_designation.setError("Please enter designation");
                            input_comittee_designation.setError("Please enter committee designation");
                            input_date_of_join.setError("Please enter doj");
                            input_mobile_number.setError("Please enter mobile");
                        }

            }
        });

//********************** Button Click Event *********************

//**********************Cancel Button Click Event *********************

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hsintent = new Intent(AddIcsCommitteeMembersActivity.this,ListOfIcsCommitteeMembersAddNewActivity
                        .class);
                startActivity(hsintent);
                finish();
            }
        });

//********************** Button Click Event *********************

//**********************Home Button Click Event *********************

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hsintent = new Intent(AddIcsCommitteeMembersActivity.this,SearchActivity
                        .class);
                startActivity(hsintent);
                finish();
            }
        });

//**********************Home Button Click Event *********************

//***********************For Popup Menu******************************
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AddIcsCommitteeMembersActivity.this,btnMenu);
                //Inflating the Popup using xml file

                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(AddIcsCommitteeMembersActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(AddIcsCommitteeMembersActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){


                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");
                            Intent hsintent = new Intent(AddIcsCommitteeMembersActivity.this,LoginActivity
                                    .class);
                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");
                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//***********************End Popup Menu******************************

    }//*******On Create Method End Here*********

    private void addCommitteeMember(String pin,String first_name,String last_name,
                                    String program,String company_designation,
                                    String committee_designation,String doj,String mobile) {

        try{

            JSONObject paramObject = new JSONObject();
            paramObject.put("pin", pin);
            paramObject.put("first_name", first_name);
            paramObject.put("last_name", last_name);
            paramObject.put("program_name", program);
            paramObject.put("company_designation", company_designation);
            paramObject.put("committee_designation", committee_designation);
            paramObject.put("doj", doj);
            paramObject.put("mobile", mobile);

            Log.e("Add Member Request JSON : ",paramObject.toString());

            addMemberAPIService.requestForAddMember(paramObject.toString()).enqueue(new Callback<AddMemberResponse>() {
                @Override
                public void onResponse(Call<AddMemberResponse> call, Response<AddMemberResponse> response) {
                    if(response.isSuccessful()){

                    }else{
                        Toast.makeText(AddIcsCommitteeMembersActivity.this, "Request for add " +
                                        "member is not successful",
                                Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<AddMemberResponse> call, Throwable t) {

                }
            });
        }catch (Exception ex){}



    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent(AddIcsCommitteeMembersActivity.this,
                    ListOfIcsCommitteeMembersAddNewActivity.class);
            startActivity(intent);
            finish();

            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

}
