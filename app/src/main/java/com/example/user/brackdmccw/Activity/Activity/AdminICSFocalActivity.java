package com.example.user.brackdmccw.Activity.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import java.util.ArrayList;

public class AdminICSFocalActivity extends Activity {


    Button btn_back;
    Button btn_home;
    Button buttonHome;
    Button btnMenu;
    Button btnSearch;

    Spinner spDistrict;
    Spinner spUpazilla;

    ArrayList<String> districtList;
    ArrayList<String> upazillzList;

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_admin_icsfocal);

        databaseHandler = new DatabaseHandler(this);
//******************** Hide Actionbar**************

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // getSupportActionBar().hide();

//******************** Hide Actionbar**************

//*********************** View Initialize ********************

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_home = (Button) findViewById(R.id.btn_home);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        btnSearch = (Button) findViewById(R.id.btnSearch);

        spDistrict = (Spinner) findViewById(R.id.spDistrict);
        spUpazilla = (Spinner) findViewById(R.id.spUpazilla);


//***********************End View Initialize ********************

//*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************




//******************* Set District List Dropdown Here ****************

        districtList = new ArrayList<>();
        districtList.add("Select District");
        districtList.add("Dhaka");
        districtList.add("Rajsahi");
        districtList.add("Barisal");

        ArrayAdapter<String> districtListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,districtList);
        districtListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDistrict.setAdapter(districtListAdapter);

//******************* End Set District List Dropdown Here ***************

//******************* Set Upazilla List Dropdown Here *******************

        upazillzList = new ArrayList<>();
        upazillzList.add("Select Upazilla");
        upazillzList.add("Uttara");
        upazillzList.add("Sadar");
        upazillzList.add("Rampura");

        ArrayAdapter<String> upazillzListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,upazillzList);
        upazillzListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUpazilla.setAdapter(upazillzListAdapter);

//********************* End Set Upazilla List Dropdown Here *******************

//**********************Search Button Click Event *********************
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
//*******************End Search Button Click Event *********************
//**********************Home Button Click Event *********************
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminICSFocalActivity.this,SearchActivity
                        .class);
                startActivity(intent);
                finish();
            }
        });
//*******************End Search Button Click Event *********************

//***********************For Popup Menu******************************

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AdminICSFocalActivity.this,btnMenu);
                //Inflating the Popup using xml file

                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(AdminICSFocalActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(AdminICSFocalActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){
                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");
                            Intent hsintent = new Intent(AdminICSFocalActivity.this,LoginActivity
                                    .class);
                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");
                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

//***********************End Popup Menu******************************


    }//********ON CREATE MRTHOD END HERE**************

    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }



}
