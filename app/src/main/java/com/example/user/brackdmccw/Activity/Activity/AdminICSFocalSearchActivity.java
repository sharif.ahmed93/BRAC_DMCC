package com.example.user.brackdmccw.Activity.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import java.util.ArrayList;

public class AdminICSFocalSearchActivity extends AppCompatActivity {

    Spinner spDistrict;
    Spinner spUpazilla;
    Button btnSearch;
    Button buttonHome;
    Button btn_back;
    ArrayList<String> districtList;
    ArrayList<String> upazillaList;
    Button btnMenu;
    ArrayList<String> upazillaIdList;
    String districtNameFromDB = "";
    String districtIDFromDB = "";


    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_icsfocal_search);

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        //View Initialize Here
        spDistrict = (Spinner) findViewById(R.id.spDistrict);
        spUpazilla = (Spinner) findViewById(R.id.spUpazilla);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btn_back = (Button) findViewById(R.id.btn_back);
        btnMenu = (Button) findViewById(R.id.btnMenu);

        databaseHandler = new DatabaseHandler(this);
        //End View Initialize Here

        //*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************


        districtList = new ArrayList<>();
        upazillaIdList = new ArrayList<>();
        //districtList.add("Select District (Fixed)");

//********************** Get Information From DB By PIN *********************

        String districtNameQry = "SELECT * FROM dmcc_personal_information " +
                " INNER JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                " INNER JOIN district ON district.id = dmcc_ics_udmt.district_id " +
                " WHERE dmcc_personal_information.pin = "+"'"+ getPreference("pin")+"'";


        try {
            Cursor districtNameCursor = databaseHandler.rawQuery(districtNameQry);
            if (districtNameCursor != null) {
                if (districtNameCursor.moveToFirst()) {
                    do {

                        districtNameFromDB = districtNameCursor.getString(districtNameCursor.getColumnIndex("district_name"));
                        districtIDFromDB = districtNameCursor.getString(districtNameCursor.getColumnIndex("district_id"));

                        TempSaveData.SelectDistrictNameFromFocalSearch = districtNameFromDB;

                        Log.e("District Name : ",districtNameFromDB);
                        Log.e("District Id : ",districtIDFromDB);
                        districtList.add(districtNameFromDB);

                    } while (districtNameCursor.moveToNext());
                }
            }
        }catch (Exception ex){
            Log.e("Query Error",ex.getMessage());
        }

//**********************End Get Information From DB By PIN *********************


//*****************************Set District List Dropdown Here*********************************


        //spDistrict.setSelection(((ArrayAdapter<String>)spDistrict.getAdapter()).getPosition(deposit_type));

        ArrayAdapter<String> districtListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,districtList);
        districtListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDistrict.setAdapter(districtListAdapter);

//*****************************End Set District List Dropdown Here*********************************

        //**************** Parse Upazilla By District Select ****************************

        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                TempSaveData.SelectDistrictIdFromFocalSearch = districtIDFromDB;
                ParseUpazillaByDistrictId(districtIDFromDB);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });



        //**************** End Parse Upazilla By District Select *************************


//*****************************Set Upazilla List Dropdown Here*************************************
        upazillaList = new ArrayList<>();
//        upazillzList.add("Select Upazilla");
//        upazillzList.add("Uttara");
//        upazillzList.add("Sadar");
//        upazillzList.add("Rampura");
//
//        ArrayAdapter<String> upazillzListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,upazillzList);
//        upazillzListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spUpazilla.setAdapter(upazillzListAdapter);

//******************************End Set Upazilla List Dropdown Here*******************************

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminICSFocalSearchActivity.this, SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminICSFocalSearchActivity.this, SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //Search button event occur here and two of theme are select then it goes to ListOfUdmtMembersActivity
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if((spDistrict.getSelectedItem().toString().trim() != "Select District")&&(spUpazilla.getSelectedItem().toString().trim() == "Select Upazilla")) {

                    TempSaveData.SelectDistrictNameFromFocalSearch = spDistrict.getSelectedItem().toString().trim();

                    Intent intent = new Intent(AdminICSFocalSearchActivity.this, ListOfIcsCommitteeMembersAddNewActivity.class);
                    intent.putExtra("Activity","AdminICSFocalSearchActivity");
                    startActivity(intent);
                    finish();

                }else{

                    TempSaveData.SelectDistrictNameFromFocalSearch = spDistrict.getSelectedItem().toString().trim();
                    TempSaveData.SelectUpazillatNameFromFocalSearch = spUpazilla.getSelectedItem().toString().trim();

                    Intent intent = new Intent(AdminICSFocalSearchActivity.this, ListOfUdmtMemberAddNewActivity.class);
                    intent.putExtra("Activity","AdminICSFocalSearchActivity");
                    startActivity(intent);
                    finish();
                }
            }
        });



//***********************For Popup Menu******************************

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AdminICSFocalSearchActivity.this,btnMenu);
                //Inflating the Popup using xml file


                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(AdminICSFocalSearchActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(AdminICSFocalSearchActivity.this,MemberProfileUpdateWithPhotoActivity.class);
                            hsintent.putExtra("Activity","AdminICSFocalSearchActivity");
                            TempSaveData.ScreenName = "AdminICSFocalSearchActivity";
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){
                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");
                            Intent hsintent = new Intent(AdminICSFocalSearchActivity.this,LoginActivity.class);
                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");

                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

//***********************End Popup Menu******************************


    }//*******On Create Method End Here*********


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }

    private void ParseUpazillaByDistrictId(String districtIdSelectFromSpinner) {

        upazillaList.clear();
        upazillaIdList.clear();
        upazillaList.add("Select Upazilla");
        upazillaIdList.add("00");
        Cursor cursorUpazilla = databaseHandler.rawQuery("SELECT * FROM upazilla WHERE district_id = "+"'"+districtIdSelectFromSpinner+"'");

        if(cursorUpazilla!=null)
        {
            if(cursorUpazilla.moveToFirst())
            {

                do{

                    upazillaIdList.add(String.valueOf(cursorUpazilla.getInt(cursorUpazilla.getColumnIndex("id"))));
                    upazillaList.add(cursorUpazilla.getString(cursorUpazilla.getColumnIndex
                            ("upazilla_name")));

                }
                while (cursorUpazilla.moveToNext());
            }
        }

        try{
            ArrayAdapter<String> upazillzListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,upazillaList);
            upazillzListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spUpazilla.setAdapter(upazillzListAdapter);
            spUpazilla.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    TempSaveData.SelectUpazillatIdFromFocalSearch = upazillaIdList.get(position);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch (Exception ex){

        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            Intent idd = new Intent(AdminICSFocalSearchActivity.this, SearchActivity.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

}
