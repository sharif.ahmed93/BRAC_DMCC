package com.example.user.brackdmccw.Activity.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AdminIcsUdmtLoginActivity extends AppCompatActivity {

    EditText input_pin;
    EditText input_name;
    Button input_dob;

    Button btnMenu;
    Button btn_login;

    DatabaseHandler databaseHandler;

    String designationFromDB = "";
    String firstNameFromDB = "";
    String memberTypeIdFromDB = "";
    String dobFromDB = "";

    static final int DIALOG_ID = 0;

    int year_x;
    int month_x;
    int day_x;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_udmt_login);

//******************** Hide Actionbar**************

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

//******************** Hide Actionbar**************

//*********************** View Initialize ********************

        input_pin = (EditText) findViewById(R.id.input_pin);
        input_name = (EditText) findViewById(R.id.input_name);

        input_dob = (Button) findViewById(R.id.input_dob);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        btn_login = (Button) findViewById(R.id.btn_login);

        databaseHandler = new DatabaseHandler(this);
//***********************End View Initialize ********************

//**************************** For Calender Current Date ***************************************************

        Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);

//**************************** For Calender Current Date ***************************************************

//*************************** InputDob Button Click Event Occur**********************************

        input_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_ID);
            }
        });
//***************************End InputDob Button Click Event Occur**********************************

//******************* When Enter Pin Data Parse From Db And Display The EditText **********
        input_pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                input_name.setText("First Name");

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//********************** Get Information From DB By PIN *********************

                String pinDataQry = "SELECT * FROM dmcc_personal_information" +
                        " INNER JOIN dmcc_ics_udmt ON dmcc_personal_information.id = " +
                        " dmcc_ics_udmt.personal_information_id" +
                        " INNER JOIN dmcc_designation ON dmcc_designation.id = " +
                        " dmcc_personal_information.designation_id" +
                        " WHERE pin = "+"'"+s.toString()+"'";

                try{
                    Cursor pinDataCursor = databaseHandler.rawQuery(pinDataQry);
                    if (pinDataCursor != null){
                        if (pinDataCursor.moveToFirst()){
                            do{
                                firstNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex
                                        ("first_name"));
                                input_name.setText(firstNameFromDB);
                                designationFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex
                                        ("designation"));
                                dobFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex
                                        ("dob"));
                                memberTypeIdFromDB = String.valueOf(pinDataCursor.getInt(pinDataCursor.getColumnIndex
                                        ("member_type_id")));

                            }while (pinDataCursor.moveToNext());
                        }
                    }
                }catch (Exception ex){}


//**********************End Get Information From DB By PIN *********************
            }

            @Override
            public void afterTextChanged(Editable s) {

//********************** Get Information From DB By PIN *********************

                String pinDataQry = "SELECT * FROM dmcc_personal_information" +
                        " INNER JOIN dmcc_ics_udmt ON dmcc_personal_information.id = " +
                        " dmcc_ics_udmt.personal_information_id" +
                        " INNER JOIN dmcc_designation ON dmcc_designation.id = " +
                        " dmcc_personal_information.designation_id" +
                        " WHERE pin = "+"'"+s.toString()+"'";

                try {
                    Cursor pinDataCursor = databaseHandler.rawQuery(pinDataQry);
                    if (pinDataCursor != null) {
                        if (pinDataCursor.moveToFirst()) {
                            do {
                                firstNameFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex
                                        ("first_name"));
                                input_name.setText(firstNameFromDB);
                                designationFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex
                                        ("designation"));
                                dobFromDB = pinDataCursor.getString(pinDataCursor.getColumnIndex
                                        ("dob"));
                                memberTypeIdFromDB = String.valueOf(pinDataCursor.getInt(pinDataCursor.getColumnIndex
                                        ("member_type_id")));

                            } while (pinDataCursor.moveToNext());
                        }
                    }
                }catch (Exception ex){}

//**********************End Get Information From DB By PIN *********************

            }//End afterTextChanged Method

        });//End addTextChangedListener

//******************* When Enter Pin Data Parse From Db And Display The EditText **********



//**********************Login Button Click Event *********************

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(input_pin.getText().toString())){

                    input_pin.setError("Please enter pin");

                }else if(input_name.getText().toString().equals("First Name")){

                    input_name.setError("Please enter name");

                }else if(input_dob.getText().equals("")){

                    input_dob.setError("Please enter dob");

                }else if (input_dob.getText().equals("") && input_name.getText().toString()
                        .equals("First Name") && TextUtils.isEmpty(input_pin.getText().toString())){

                    input_name.setError("Please enter name");
                    input_pin.setError("Please enter pin");
                    input_dob.setError("Please enter dob");

                }
                else{
                    if (designationFromDB.equals("Admin") &&  dateFormatChange(input_dob.getText().toString()).equals(dobFromDB)){

                        TempSaveData.AdminIcsUdmtLoginPin = input_pin.getText().toString();
                        TempSaveData.AdminIcsUdmtLoginName = input_name.getText().toString();
                        TempSaveData.AdminIcsUdmtLoginCMTYDesig = "Admin";

                        Intent intent = new Intent(AdminIcsUdmtLoginActivity.this,MemberProfileUpdateWithPhotoActivity.class);
                        intent.putExtra("Activity","AdminIcsUdmtLoginActivity");
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(AdminIcsUdmtLoginActivity.this, "You are not admin", Toast
                                .LENGTH_SHORT).show();
                    }

                }

            }
        });

//**********************End Login Button Click Event *********************



//***********************For Popup Menu******************************
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AdminIcsUdmtLoginActivity.this,btnMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(AdminIcsUdmtLoginActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(AdminIcsUdmtLoginActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){

                            Intent hsintent = new Intent(AdminIcsUdmtLoginActivity.this,LoginActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//***********************End Popup Menu******************************

    }//*******On Create Method End Here*********


    //**********************************For DatePicker dialog**********************************************
    @Override
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_ID){
            DatePickerDialog datePickerDialog =  new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
            return datePickerDialog;
        }
        else{
            return null;
        }

    }
    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            year_x = year;
            // month_x = month + 1;
            month_x = month;
            day_x = day;

            //etDepositDate.setText(day_x+"-"+month_x+"-"+year_x);
            input_dob.setText(formatDate(year_x,month_x,day_x));
        }
    };

    private static String formatDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
       // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(date);
    }
    //**********************************End DatePicker dialog**********************************************


   public String dateFormatChange(String date){

       //String ds1 = "2007-06-30";
       SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
       SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
       String ds2 = null;
       try {
           ds2 = sdf2.format(sdf1.parse(date));
       } catch (ParseException e) {
           e.printStackTrace();
       }
       System.out.println(ds2); //will be 30/06/2007

       return ds2;
   }
}
