package com.example.user.brackdmccw.Activity.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.user.brackdmccw.Activity.Adapter.ListOfIcsCommitteeMembersAdapter;
import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class ListOfIcsCommitteeMembersActivity extends AppCompatActivity {


    ListView listOfICSCommittee;
    Button backBtn;
    Button btnMenu;
    Button buttonHome;
    TextView tvDistrict;
    TextView tvUpdateDate;

    DatabaseHandler databaseHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_ics_committee_members);

//******************** Hide Actionbar**************

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

//******************** Hide Actionbar**************

//*********************** View Initialize ********************

        listOfICSCommittee = (ListView) findViewById(R.id.listOfICSCommittee);
        backBtn = (Button) findViewById(R.id.btn_back);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        tvDistrict = (TextView) findViewById(R.id.tvDistrict);
        tvUpdateDate = (TextView) findViewById(R.id.tvUpdateDate);

        tvDistrict.setText("District : "+TempSaveData.SelectDistrictName);

        databaseHandler = new DatabaseHandler(this);

//***********************End View Initialize ********************

        //*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************



//**********************Get Intent Data From SearchActivity***********
        final String getIntentData = getIntent().getStringExtra("Activity");
//**********************Get Intent From Other Activity***********

//************************ Get LastUpdate Date From DB***************
        try{
            String lastUpdateDateQry = "SELECT updated_at FROM dmcc_ics_udmt WHERE " +
                    "committee_type_id = 1 ";

            Cursor cursorLastUpdateDate = databaseHandler.rawQuery(lastUpdateDateQry);
            if(cursorLastUpdateDate != null){
                if (cursorLastUpdateDate.moveToFirst()){
                    do{
                        //String last_udate_date = cursorLastUpdateDate.getString(cursorLastUpdateDate
                        //        .getColumnIndex("updated_at"));
                        String last_udate_date = cursorLastUpdateDate.getString(0);
                        java.sql.Timestamp ts = java.sql.Timestamp.valueOf(last_udate_date) ;
                        String date = dateFormatChangeTo(ts);

                        tvUpdateDate.setText("Update Date : "+date);


                    }while (cursorLastUpdateDate.moveToNext());
                }
            }
            Log.e("SELECT updated_at FROM dmcc_ics_udmt WHERE committe_type_id = 1","NONE");

        }catch (Exception ex){

        }
//************************End Get LastUpdate From DB***************

//**************** Back Button Click Event Occur *******************
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (getIntentData.equals("SearchActivity")){
//                    Intent intent = new Intent(ListOfIcsCommitteeMembersActivity.this,SearchActivity.class);
//                    startActivity(intent);
//                }
                Intent intent = new Intent(ListOfIcsCommitteeMembersActivity.this,SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });
//****************End Back Button Click Event Occur *******************

//**************** Home Button Click Event Occur *******************
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfIcsCommitteeMembersActivity.this,SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });
//****************End Home Button Click Event Occur *******************


//***********************For Popup Menu******************************
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ListOfIcsCommitteeMembersActivity.this,btnMenu);
                //Inflating the Popup using xml file

                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(ListOfIcsCommitteeMembersActivity.this,AdminICSFocalSearchActivity.class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(ListOfIcsCommitteeMembersActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            TempSaveData.ScreenName = "ListOfIcsCommitteeMembersActivity";
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){
                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");
                            Intent hsintent = new Intent(ListOfIcsCommitteeMembersActivity.this,LoginActivity.class);
                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");
                            startActivity(hsintent);
                            finish();

                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//***********************End Popup Menu******************************


//************GET Data List Of Ics Committee Members Data From API/DATABASE*****************

        ArrayList<HashMap<String,String>> itemListFromLocalDb = new ArrayList<>();

//*********************** Select ICS Memeber Whoose Committee Type Is ICS*****************************

        String chngeUpdQry = "SELECT * FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON " +
                "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "LEFT JOIN dmcc_committee_designation ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id " +
                "WHERE dmcc_ics_udmt.district_id = "+"'"+TempSaveData.SelectDistrictId+"'"+" AND " +
                "dmcc_ics_udmt.committee_type_id = 1";

//        String qry = "SELECT * FROM dmcc_personal_information LEFT JOIN dmcc_ics_udmt ON " +
//                "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
//                "LEFT JOIN dmcc_designation ON dmcc_designation.id = dmcc_personal_information.designation_id " +
//                "WHERE dmcc_ics_udmt.district_id = "+"'"+TempSaveData.SelectDistrictId+"'"+" AND " +
//                "dmcc_ics_udmt.committee_type_id = 1 ";
//
//        String query = "SELECT * FROM dmcc_ics_udmt LEFT JOIN dmcc_personal_information ON " +
//                "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id WHERE " +
//                "district_id = "+"'"+TempSaveData.SelectDistrictId+"'"+" AND member_type_id = 1 ";

        int sl_no = 0;
        Log.e("SL No : ",String.valueOf(sl_no));
        Cursor cursorPersonalInfo = databaseHandler.rawQuery(chngeUpdQry);
        if(cursorPersonalInfo!=null){
            if(cursorPersonalInfo.moveToFirst()){
                do{

                    sl_no++;

                    Log.e("SL No : ",String.valueOf(sl_no));

                    String first_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("first_name"));
                    String last_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("last_name"));
                    String name = first_name+" "+last_name;
                    String id = String.valueOf(cursorPersonalInfo.getInt(cursorPersonalInfo.getColumnIndex
                            ("id")));
                    String pic_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pic_name"));
                    String dob = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("dob"));
                    String doj = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("doj"));
                    String pin = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pin"));
                    String program_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("program_name"));
                    String committee_designation = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("committee_designation"));
                    String company_designation = cursorPersonalInfo.getString(cursorPersonalInfo
                            .getColumnIndex("company_designation"));
                    String phone = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("phone"));
                    String mobile = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("mobile"));



                    HashMap<String,String> singleItemMap = new HashMap<>();
                    singleItemMap.put("sl_no", String.valueOf(sl_no));
                    singleItemMap.put("id", id);
                    singleItemMap.put("first_name",first_name);
                    singleItemMap.put("last_name",last_name);
                    singleItemMap.put("name",name);
                    singleItemMap.put("pin",pin);
                    singleItemMap.put("committee_designation","ICP");
                    singleItemMap.put("programme",program_name);
                    singleItemMap.put("company_designation",company_designation);
                    singleItemMap.put("committee_designation",committee_designation);
                    singleItemMap.put("mobile_number",mobile);

                    itemListFromLocalDb.add(singleItemMap);

                    Log.e("PI : ",singleItemMap.toString());


                }while(cursorPersonalInfo.moveToNext());
            }

        }



//*********************** Select ICS Memeber Whoose Member Type Is ICS *****************************



        Log.e("Select District ID : ",String.valueOf(TempSaveData.SelectDistrictId));

        ListOfIcsCommitteeMembersAdapter listOfIcsCommitteeMembersAdapter = new
                ListOfIcsCommitteeMembersAdapter(ListOfIcsCommitteeMembersActivity.this,
                itemListFromLocalDb);
        listOfICSCommittee.setAdapter(listOfIcsCommitteeMembersAdapter);

//************GET Data List Of Ics Committee Members Data From API/DATABASE*****************

    }//*******On Create Method End Here*********
    public String dateFormatChangeTo(Timestamp timestamp){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return format.format(timestamp);
    }


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            Intent idd = new Intent(ListOfIcsCommitteeMembersActivity.this, SearchActivity.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
