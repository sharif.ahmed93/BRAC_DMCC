package com.example.user.brackdmccw.Activity.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.example.user.brackdmccw.Activity.Adapter.ListOfIcsCommitteeMembersAdapter;
import com.example.user.brackdmccw.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ListOfIcsCommitteeMembersSaveActivity extends AppCompatActivity {

    Button btnMenu;
    Button buttonHome;
    Button saveBtn;
    ListView listOfICSCommittee;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_of_isc_committe_members_save_cancel);

        //******************** Hide Actionbar**************

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

//******************** Hide Actionbar**************

//*********************** View Initialize ********************

        btnMenu = (Button) findViewById(R.id.btnMenu);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        listOfICSCommittee = (ListView) findViewById(R.id.listOfICSCommittee);


//***********************End View Initialize ********************

//**********************Save Button Click Event *********************

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfIcsCommitteeMembersSaveActivity.this,ListOfIcsCommitteeMembersAddNewActivity
                        .class);
                startActivity(intent);
                finish();
            }
        });

//**********************End Save Button Click Event *********************

//**********************Home Button Click Event *********************

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfIcsCommitteeMembersSaveActivity.this,SearchActivity
                        .class);
                startActivity(intent);
                finish();
            }
        });

//**********************End Home Button Click Event *********************

//***********************For Popup Menu******************************
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ListOfIcsCommitteeMembersSaveActivity.this,btnMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(ListOfIcsCommitteeMembersSaveActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(ListOfIcsCommitteeMembersSaveActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){

                            Intent hsintent = new Intent(ListOfIcsCommitteeMembersSaveActivity.this,LoginActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//***********************End Popup Menu******************************


//************GET Data List Of Ics Committee Members Data From API/DATABASE*****************

        ArrayList<HashMap<String,String>> itemListFromApi = new ArrayList<>();
        HashMap<String,String> singleItemMap1 = new HashMap<>();
        singleItemMap1.put("sl_no","1");
        singleItemMap1.put("name","Arena Phone");
        singleItemMap1.put("pin","123456");
        singleItemMap1.put("committee_designation","SOP");
        singleItemMap1.put("programme","Campaign");
        singleItemMap1.put("designation","Admin");
        singleItemMap1.put("mobile_number","01823423377");
        itemListFromApi.add(singleItemMap1);

        HashMap<String,String> singleItemMap2 = new HashMap<>();
        singleItemMap2.put("sl_no","2");
        singleItemMap2.put("name","AD65 Ltd");
        singleItemMap2.put("pin","123456");
        singleItemMap2.put("committee_designation","ICP");
        singleItemMap2.put("programme","Campaign");
        singleItemMap2.put("designation","Admin");
        singleItemMap2.put("mobile_number","01855523377");
        itemListFromApi.add(singleItemMap2);

        HashMap<String,String> singleItemMap3 = new HashMap<>();
        singleItemMap3.put("sl_no","3");
        singleItemMap3.put("name","AD65 Ltd");
        singleItemMap3.put("pin","123456");
        singleItemMap3.put("committee_designation","ICP");
        singleItemMap3.put("programme","Campaign");
        singleItemMap3.put("designation","Admin");
        singleItemMap3.put("mobile_number","01855523377");
        itemListFromApi.add(singleItemMap3);

        HashMap<String,String> singleItemMap4 = new HashMap<>();
        singleItemMap4.put("sl_no","4");
        singleItemMap4.put("name","AD65 Ltd");
        singleItemMap4.put("pin","123456");
        singleItemMap4.put("committee_designation","ICP");
        singleItemMap4.put("programme","Campaign");
        singleItemMap4.put("designation","Admin");
        singleItemMap4.put("mobile_number","01855523377");
        itemListFromApi.add(singleItemMap4);


        HashMap<String,String> singleItemMap7 = new HashMap<>();
        singleItemMap7.put("sl_no","5");
        singleItemMap7.put("name","AD65 Ltd");
        singleItemMap7.put("pin","123456");
        singleItemMap7.put("committee_designation","ICP");
        singleItemMap7.put("programme","Campaign");
        singleItemMap7.put("designation","Admin");
        singleItemMap7.put("mobile_number","01855523377");
        itemListFromApi.add(singleItemMap7);


        HashMap<String,String> singleItemMap5 = new HashMap<>();
        singleItemMap5.put("sl_no","6");
        singleItemMap5.put("name","AD65 Ltd");
        singleItemMap5.put("pin","123456");
        singleItemMap5.put("committee_designation","ICP");
        singleItemMap5.put("programme","Campaign");
        singleItemMap5.put("designation","Admin");
        singleItemMap5.put("mobile_number","01855523377");
        itemListFromApi.add(singleItemMap5);


        HashMap<String,String> singleItemMap6 = new HashMap<>();
        singleItemMap6.put("sl_no","7");
        singleItemMap6.put("name","AD65 Ltd");
        singleItemMap6.put("pin","123456");
        singleItemMap6.put("committee_designation","ICP");
        singleItemMap6.put("programme","Campaign");
        singleItemMap6.put("designation","Admin");
        singleItemMap6.put("mobile_number","01855523377");
        itemListFromApi.add(singleItemMap6);


        ListOfIcsCommitteeMembersAdapter listOfIcsCommitteeMembersAdapter = new
                ListOfIcsCommitteeMembersAdapter(ListOfIcsCommitteeMembersSaveActivity.this,
                itemListFromApi);
        listOfICSCommittee.setAdapter(listOfIcsCommitteeMembersAdapter);

//************GET Data List Of Ics Committee Members Data From API/DATABASE*****************

    }//*******On Create Method End Here*********
}
