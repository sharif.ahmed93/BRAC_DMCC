package com.example.user.brackdmccw.Activity.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.user.brackdmccw.Activity.Adapter.ListOfUdmtCommitteeMembersAddNewAdapter;
import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class ListOfUdmtMemberAddNewActivity extends AppCompatActivity {

    Button btnMenu;
    Button btn_back;
    Button buttonHome;
    TextView btnAddNew;
    TextView tvUpdateDate;
    TextView tvDistrict;
    TextView tvUpazilla;
    ListView listOfUDMTCommittee;
    DatabaseHandler databaseHandler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_udmt_member_add_new);


//******************** Hide Actionbar**************
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();


//******************** Hide Actionbar**************

//*********************** View Initialize ********************

        btnMenu = (Button) findViewById(R.id.btnMenu);
        btn_back = (Button) findViewById(R.id.btn_back);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btnAddNew = (TextView) findViewById(R.id.btnAddNew);
        tvUpdateDate = (TextView) findViewById(R.id.tvUpdateDate);
        tvDistrict = (TextView) findViewById(R.id.tvDistrict);
        tvUpazilla = (TextView) findViewById(R.id.tvUpazilla);
        listOfUDMTCommittee = (ListView) findViewById(R.id.listOfUDMTCommittee);
        databaseHandler = new DatabaseHandler(this);
//***********************End View Initialize ********************



//*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************

        tvDistrict.setText("District : "+ TempSaveData.SelectDistrictNameFromFocalSearch);
        tvUpazilla.setText("District : "+ TempSaveData.SelectUpazillatNameFromFocalSearch);




//************************ Get LastUpdate Date From DB***************
        try{
            String lastUpdateDateQry = "SELECT updated_at FROM dmcc_ics_udmt WHERE " +
                    "committee_type_id = 2 ";

            Cursor cursorLastUpdateDate = databaseHandler.rawQuery(lastUpdateDateQry);
            if(cursorLastUpdateDate != null){
                if (cursorLastUpdateDate.moveToFirst()){
                    do{
                        //String last_udate_date = cursorLastUpdateDate.getString(cursorLastUpdateDate
                        //        .getColumnIndex("updated_at"));
                        String last_udate_date = cursorLastUpdateDate.getString(0);
                        java.sql.Timestamp ts = java.sql.Timestamp.valueOf(last_udate_date) ;
                        String date = dateFormatChangeTo(ts);

                        tvUpdateDate.setText("Update Date : "+date);


                    }while (cursorLastUpdateDate.moveToNext());
                }
            }
            Log.e("SELECT updated_at FROM dmcc_ics_udmt WHERE committe_type_id = 2","NONE");

        }catch (Exception ex){

        }
//************************End Get LastUpdate From DB***************



//**********************Add New Button Click Event *********************
        btnAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfUdmtMemberAddNewActivity.this,AddIcsCommitteeMembersActivity.class);
                startActivity(intent);
                finish();
            }
        });

//**********************End Add New Button Click Event *********************

//**********************Home Button Click Event *********************
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfUdmtMemberAddNewActivity.this,SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });

//**********************End Home Button Click Event *********************

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfUdmtMemberAddNewActivity.this,
                        AdminICSFocalSearchActivity.class);
                startActivity(intent);
                finish();
            }
        });


//***********************For Popup Menu******************************
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ListOfUdmtMemberAddNewActivity.this,btnMenu);
                //Inflating the Popup using xml file


                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(ListOfUdmtMemberAddNewActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(ListOfUdmtMemberAddNewActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            TempSaveData.ScreenName = "ListOfUdmtMemberAddNewActivity";
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){
                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");
                            Intent hsintent = new Intent(ListOfUdmtMemberAddNewActivity.this,LoginActivity.class);
                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");
                            startActivity(hsintent);
                            finish();

                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//***********************End Popup Menu******************************



//************GET Data List Of Ics Committee Members Data From API/DATABASE*****************

        ArrayList<HashMap<String,String>> itemListFromLocalDb = new ArrayList<>();


//******************Select UDMT Memeber Whoose Committee Type Is UDMT************************

        String chngeUpdQry = "SELECT dmcc_personal_information.*,dmcc_committee_designation.committee_designation FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON " +
                "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "LEFT JOIN dmcc_committee_designation ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id " +
                "WHERE dmcc_ics_udmt.district_id = "+"'"+TempSaveData.SelectDistrictIdFromFocalSearch+"'"+" " +
                "AND dmcc_ics_udmt.upazila_id = "+"'"+TempSaveData.SelectUpazillatIdFromFocalSearch+"'"+" " +
                "AND dmcc_ics_udmt.committee_type_id = 2 ";

        int sl_no = 0;
        Log.e("SL No : ",String.valueOf(sl_no));
        Cursor cursorPersonalInfo = databaseHandler.rawQuery(chngeUpdQry);
        if(cursorPersonalInfo!=null){
            if(cursorPersonalInfo.moveToFirst()){
                do{

                    sl_no++;

                    Log.e("SL No : ",String.valueOf(sl_no));

                    String first_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("first_name"));
                    String last_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("last_name"));
                    String name = first_name+" "+last_name;
                    String id = String.valueOf(cursorPersonalInfo.getInt(cursorPersonalInfo.getColumnIndex("id")));
                    String pic_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pic_name"));
                    String dob = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("dob"));
                    String doj = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("doj"));
                    String pin = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pin"));
                    String program_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("program_name"));
                    String committee_designation = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("committee_designation"));
                    String company_designation = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("company_designation"));
                    String phone = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("phone"));
                    String mobile = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("mobile"));



                    HashMap<String,String> singleItemMap = new HashMap<>();
                    singleItemMap.put("sl_no", String.valueOf(sl_no));
                    singleItemMap.put("name",name);
                    singleItemMap.put("pin",pin);
                    singleItemMap.put("pi_id", id);
                    singleItemMap.put("first_name",first_name);
                    singleItemMap.put("last_name",last_name);
                    singleItemMap.put("pic_name",pic_name);
                    singleItemMap.put("committee_designation",committee_designation);
                    singleItemMap.put("doj",doj);
                    singleItemMap.put("programme",program_name);
                    singleItemMap.put("company_designation",company_designation);
                    singleItemMap.put("committee_designation",committee_designation);
                    singleItemMap.put("mobile_number",mobile);

                    itemListFromLocalDb.add(singleItemMap);

                    Log.e("PI : ",singleItemMap.toString());
                    Log.e("List Of ICS ADD New PI Id: ",id);


                }while(cursorPersonalInfo.moveToNext());
            }

        }



//***********Select UDMT Memeber Whoose Member Type Is UDMT ******************



        ListOfUdmtCommitteeMembersAddNewAdapter listOfUdmtCommitteeMembersAddNewAdapter = new
                ListOfUdmtCommitteeMembersAddNewAdapter(ListOfUdmtMemberAddNewActivity.this,
                itemListFromLocalDb);
        listOfUDMTCommittee.setAdapter(listOfUdmtCommitteeMembersAddNewAdapter);

//************GET Data List Of UDMT Committee Members Data From API/DATABASE*****************


    }//On Create Method


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }


    public String dateFormatChangeTo(Timestamp timestamp){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return format.format(timestamp);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            Intent idd = new Intent(ListOfUdmtMemberAddNewActivity.this, AdminICSFocalSearchActivity.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
