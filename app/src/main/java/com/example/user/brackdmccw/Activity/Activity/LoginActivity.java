package com.example.user.brackdmccw.Activity.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.NetworkCall.APIUtils.ApiUtils;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.UserLoginAPIService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.UserLoginSuccessApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.LoginResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.CommitteeDesignation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.District;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.IcsUdmt;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.PersonalInformation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.Upazilla;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.UserLoginSuccessResponse;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {


    EditText etPin;
    Button etDob;
    Button btnLogin;
    LinearLayout rowLayOut;
    boolean isInternetConnect = false;

    static final int DIALOG_ID = 0;

    int year_x;
    int month_x;
    int day_x;

    private UserLoginAPIService userLoginAPIService;
    private UserLoginSuccessApiService userLoginSuccessApiService;
    private UserLoginSuccessResponse userLoginSuccessResponse;

    ArrayList<District>districtList = new ArrayList<District>();
    ArrayList<Upazilla>upazillaList = new ArrayList<Upazilla>();
    ArrayList<PersonalInformation>personalInformationList = new ArrayList<PersonalInformation>();
    ArrayList<IcsUdmt>icsUDMTList = new ArrayList<IcsUdmt>();
    ArrayList<CommitteeDesignation>committeeDesignationList = new ArrayList<CommitteeDesignation>();


    private HashMap<String,String> districtMap = new HashMap<>();
    private HashMap<String,String> upazillaMap = new HashMap<>();
    private HashMap<String,String> personalInfromationMap = new HashMap<>();
    private HashMap<String,String> icsUDMTMap = new HashMap<>();
    private HashMap<String,String> committeeDesignationMap = new HashMap<>();
    private HashMap<String,String> memberTypeMap = new HashMap<>();
    private HashMap<String,String> committeeTypeMap = new HashMap<>();


    DatabaseHandler databaseHandler;

    AlertDialog spotsDialog;
    ProgressDialog progressDialog;


    private TextView tvResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        //View Initialize
        etPin = (EditText) findViewById(R.id.input_pin);
        etDob = (Button) findViewById(R.id.input_dob);
        btnLogin = (Button) findViewById(R.id.btn_login);
        tvResponse = (TextView) findViewById(R.id.tvResponse);
        spotsDialog = new SpotsDialog(this);

        rowLayOut = (LinearLayout) findViewById(R.id.rowLayOut);
        //rowLayOut.setBackgroundColor(Color.argb(135,220,20,60));
        //End View Initialize


        progressDialog = new ProgressDialog(LoginActivity.this);


        userLoginAPIService = ApiUtils.getAPIService();
        userLoginSuccessApiService = ApiUtils.getLoginSuccessAPIService();

//***********Session Check For First Login and save pin and dob *********
        databaseHandler = new DatabaseHandler(this);

        if(getLoginStatus()){
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            finish();
        }
//***********Session Check For First Login and save pin and dob *********

        // Login Button Click Event Occur Here Go to Page To SearchActivity

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pin = etPin.getText().toString().trim();
                String dob = etDob.getText().toString().trim();
                if(!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(dob)) {

                    if(isNetworkConnected()){
                        spotsDialog.show();
                        userLogin(pin, dob);
                    }else{

                        Toast.makeText(LoginActivity.this, "Please Check Your Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }

                }else if(TextUtils.isEmpty(pin)){
                    etPin.setError("Please enter pin");
                }else if(TextUtils.isEmpty(dob)){
                    etDob.setError("Please enter dob");
                }
                else{
                    etPin.setError("Please enter pin");
                    etDob.setError("Please enter dob");
                }

            }
        });// Login Button Click Event Occur Here Go to Page To SearchActivity


        //**************************** For Calender Current Date ***************************************************

        Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);

        //etDob.setText(formatDate(year_x,month_x,day_x));

        // DOB Button Click Event Occur Here Go to Page To SearchActivity
        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_ID);
            }
        });// DOB Button Click Event Occur Here Go to Select date

    }// On Create Method End Here
    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( progressDialog!=null && progressDialog.isShowing() ){
            progressDialog.cancel();
        }
    }

//User Login Method Start
    private void userLogin(final String pin, final String dob) {

        //For Valid User
        // prepare call in Retrofit 2.0
        try {

            JSONObject paramObject = new JSONObject();
            paramObject.put("pin", pin);
            paramObject.put("dob", dob);
            //tvResponse.setText(paramObject.toString());

            userLoginAPIService.requestForLogin(paramObject.toString()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if(response.isSuccessful()){
                        //Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast
                        //        .LENGTH_SHORT).show();
                        spotsDialog.dismiss();

                        if (response.body().getStatus().equals("success")){

                            try{

                                progressDialog.setMessage("Please Waiting......");
                                progressDialog.show();

                                savePreference("pin",pin);
                                savePreference("dob",dob);

                                TempSaveData.LoginActivityLoginPin = pin;

                                userLoginSuccessApiService.requestForGetInformation().enqueue(new Callback<UserLoginSuccessResponse>() {
                                    @Override
                                    public void onResponse(Call<UserLoginSuccessResponse> call, Response<UserLoginSuccessResponse> response) {

                                        if(response.isSuccessful()){

                                            userLoginSuccessResponse = response.body();

                                            districtList = (ArrayList<District>) userLoginSuccessResponse.getDistricts();
                                            upazillaList = (ArrayList<Upazilla>) userLoginSuccessResponse.getUpazilla();
                                            personalInformationList = (ArrayList<PersonalInformation>) userLoginSuccessResponse.getPersonalInformation();
                                            icsUDMTList = (ArrayList<IcsUdmt>) userLoginSuccessResponse.getIcsUdmt();
                                            committeeDesignationList = (ArrayList<CommitteeDesignation>)
                                                    userLoginSuccessResponse.getCommitteeDesignations();

                                            for (District district : districtList) {

                                                districtMap.clear();

                                                Log.e("District List"," Division Id -> "+district.getDivisionId() + " "
                                                        +" District Name -> "+ " " +district.getDistrictName()+" "+" "
                                                        + "District Id -> "+district.getId());

                                                districtMap.put("id",district.getId());
                                                districtMap.put("division_id",district.getDivisionId());
                                                districtMap.put("district_name",district.getDistrictName());

                                                if(!databaseHandler.checkDuplicate("district", "id", district.getId()))
                                                    databaseHandler.InsertTable(districtMap,"district");
                                                else
                                                    databaseHandler.UpdateTable(districtMap, "district", "id", district.getId());

                                                Log.e("district table insert",districtMap.toString());
                                            }

                                            for (Upazilla upazilla : upazillaList) {

                                                upazillaMap.clear();

                                                Log.e("Upazilla List"," District Id -> "+upazilla
                                                        .getDistrictId() + " "
                                                        +" Upazilla Name -> "+ " " +upazilla
                                                        .getThanaName()+" "+" "
                                                        + "Upazilla Id -> "+upazilla.getId());

                                                upazillaMap.put("id",upazilla.getId());
                                                upazillaMap.put("district_id",upazilla.getDistrictId());
                                                upazillaMap.put("upazilla_name",upazilla.getThanaName());

                                                if(!databaseHandler.checkDuplicate("upazilla", "id", upazilla.getId()))
                                                    databaseHandler.InsertTable(upazillaMap,"upazilla");
                                                else
                                                    databaseHandler.UpdateTable(upazillaMap, "upazilla", "id", upazilla.getId());


                                                Log.e("upazilla table insert",upazillaMap.toString());

                                            }

                                            for (IcsUdmt icsUdmt : icsUDMTList) {

                                                icsUDMTMap.clear();

                                                Log.e("Ics UDMT List","Ics UDMT ID -> "+icsUdmt.getId
                                                        ());

                                                icsUDMTMap.put("id",icsUdmt.getId());
                                                icsUDMTMap.put("personal_information_id",icsUdmt.getPersonalInformationId());
                                                icsUDMTMap.put("division_id",icsUdmt.getDivisionId());
                                                icsUDMTMap.put("district_id",icsUdmt.getDistrictId());
                                                icsUDMTMap.put("upazila_id",icsUdmt.getUpazilaId());
                                                icsUDMTMap.put("committee_type_id",icsUdmt.getCommitteTypeId());
                                                icsUDMTMap.put("member_type_id",icsUdmt.getMemberTypeId());
                                                icsUDMTMap.put("committee_designation_id",icsUdmt
                                                        .getCommitteeDesignationId());
                                                icsUDMTMap.put("status",icsUdmt.getStatus());
                                                icsUDMTMap.put("created_by",icsUdmt.getCreatedBy());
                                                icsUDMTMap.put("updated_by",icsUdmt.getUpdatedBy());
                                                icsUDMTMap.put("created_at",icsUdmt.getCreatedAt());
                                                icsUDMTMap.put("updated_at",icsUdmt.getUpdatedAt());


                                                if(!databaseHandler.checkDuplicate("dmcc_ics_udmt", "id", icsUdmt.getId()))
                                                    databaseHandler.InsertTable(icsUDMTMap,"dmcc_ics_udmt");
                                                else
                                                    databaseHandler.UpdateTable(icsUDMTMap, "dmcc_ics_udmt", "id", icsUdmt.getId());



                                                Log.e("ics udmt table insert",icsUDMTMap.toString());

                                            }

                                            for (PersonalInformation personalInformation : personalInformationList) {

                                                personalInfromationMap.clear();

                                                Log.e("Personal Info List",personalInformation.getFirstName());

                                                personalInfromationMap.put("id",personalInformation.getId());
                                                personalInfromationMap.put("first_name",
                                                        personalInformation.getFirstName());
                                                personalInfromationMap.put("last_name",
                                                        personalInformation.getLastName());
                                                personalInfromationMap.put("pic_name",
                                                        personalInformation.getPicName());
                                                personalInfromationMap.put("dob",personalInformation.getDob());
                                                personalInfromationMap.put("doj",personalInformation
                                                        .getDoj());
                                                personalInfromationMap.put("pin",personalInformation
                                                        .getPin());
                                                personalInfromationMap.put("program_name",
                                                        personalInformation.getProgramName());
                                                personalInfromationMap.put("company_designation",
                                                        personalInformation.getCompanyDesignation());
                                                personalInfromationMap.put("phone",
                                                        personalInformation.getPhone());
                                                personalInfromationMap.put("mobile",
                                                        personalInformation.getMobile());
                                                personalInfromationMap.put("current_work_station",
                                                        personalInformation.getCurrentWorkStation());
                                                personalInfromationMap.put("created_by",
                                                        personalInformation.getCreatedBy());
                                                personalInfromationMap.put("updated_by",
                                                        personalInformation.getUpdatedBy());
                                                personalInfromationMap.put("created_at",
                                                        personalInformation.getCreatedAt());
                                                personalInfromationMap.put("updated_at",
                                                        personalInformation.getUpdatedAt());



                                                if(!databaseHandler.checkDuplicate("dmcc_personal_information", "id", personalInformation.getId()))
                                                    databaseHandler.InsertTable(personalInfromationMap,"dmcc_personal_information");
                                                else
                                                    databaseHandler.UpdateTable(personalInfromationMap, "dmcc_personal_information", "id", personalInformation.getId());


                                                Log.e("personal info table insert",personalInfromationMap.toString());
                                            }

                                            for (CommitteeDesignation committeeDesignation : committeeDesignationList) {

                                                committeeDesignationMap.clear();

                                                Log.e("Committee Designation List"," Committee Designation Id " +
                                                        "-> "+committeeDesignation
                                                        .getId() + " "
                                                        +"Committee Designation Name -> "+ " "
                                                        +committeeDesignation
                                                        .getCommitteeDesignation());

                                                committeeDesignationMap.put("id",committeeDesignation.getId());
                                                committeeDesignationMap.put
                                                        ("committee_designation",
                                                                committeeDesignation.getCommitteeDesignation());
                                                committeeDesignationMap.put("created_by",
                                                        committeeDesignation.getCreatedBy());
                                                committeeDesignationMap.put("created_at",
                                                        committeeDesignation.getCreatedAt());
                                                committeeDesignationMap.put("updated_by",
                                                        committeeDesignation.getUpdatedBy());
                                                committeeDesignationMap.put("updated_at",
                                                        committeeDesignation.getUpdatedAt());

                                                if(!databaseHandler.checkDuplicate("dmcc_committee_designation", "id", committeeDesignation.getId())){
                                                    databaseHandler.InsertTable(committeeDesignationMap,"dmcc_committee_designation");
                                                    Log.e("dmcc_committee_designation table insert",committeeDesignationMap.toString());
                                                }

                                                else{
                                                    databaseHandler.UpdateTable(committeeDesignationMap, "dmcc_committee_designation", "id", committeeDesignation.getId());
                                                    Log.e("dmcc_committee_designation table update",
                                                            committeeDesignationMap.toString());
                                                }
                                            }
                                            progressDialog.dismiss();
                                            Toast.makeText(LoginActivity.this, response.body().getStatus(), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(LoginActivity.this,SearchActivity.class);
                                            startActivity(intent);
                                            finish();


                                        }else{

                                            Toast.makeText(LoginActivity.this,"Success Api Response " +
                                                    "Not Successful", Toast
                                                    .LENGTH_SHORT).show();
                                            progressDialog.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<UserLoginSuccessResponse> call, Throwable t) {

                                        Toast.makeText(LoginActivity.this, "Did not work", Toast
                                                .LENGTH_SHORT)
                                                .show();
                                        progressDialog.dismiss();
                                    }
                                });


//                                Intent intent = new Intent(LoginActivity.this,SearchActivity.class);
//                                startActivity(intent);

                            }catch (Exception ex){}


                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                      //  tvResponse.setText(response.body().getMessage());


                    }else{
                        Toast.makeText(LoginActivity.this, "Request for login is not successful",
                                Toast
                                .LENGTH_SHORT)
                                .show();
                        spotsDialog.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("Failuer Mmethod : ","On Falure Method Call While LOGIN");
                    Toast.makeText(LoginActivity.this, " Please try again !!!!! ", Toast
                            .LENGTH_SHORT).show();
                    spotsDialog.dismiss();
//                    Log.e("Failure",t.getMessage());
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Valid User




    }//End UserLogin Method


    //**********************************For DatePicker dialog**********************************************
    @Override
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_ID){
            DatePickerDialog datePickerDialog =  new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
            return datePickerDialog;
        }
        else{
            return null;
        }

    }
    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            year_x = year;
            // month_x = month + 1;
            month_x = month;
            day_x = day;

            //etDepositDate.setText(day_x+"-"+month_x+"-"+year_x);
            etDob.setText(formatDate(year_x,month_x,day_x));
        }
    };

    private static String formatDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        return sdf.format(date);
    }
    //**********************************End DatePicker dialog**********************************************


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }

    public boolean getLoginStatus() {
        String pin = getPreference("pin");
        String dob = getPreference("dob");
        if(!pin.equals("NO PREFERENCE") && !dob.equals("NO PREFERENCE"))
            return true;
        else
            return false;
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub


        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    LoginActivity.this);

            // set dialog message
            alertDialogBuilder
                    .setMessage("Do you want to Exit it?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();


                        }
                    })
                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    public void validUserCheck(final String pin, final String dob){
        userLoginSuccessApiService.requestForGetInformation().enqueue(new Callback<UserLoginSuccessResponse>() {
            @Override
            public void onResponse(Call<UserLoginSuccessResponse> call, Response<UserLoginSuccessResponse> response) {
                if(response.isSuccessful()) {
                    UserLoginSuccessResponse userLoginSuccessResponse = new UserLoginSuccessResponse();
                    userLoginSuccessResponse = response.body();
                    ArrayList<PersonalInformation>personList = new ArrayList<PersonalInformation>();
                    personList = (ArrayList<PersonalInformation>) userLoginSuccessResponse.getPersonalInformation();
                    for (PersonalInformation personalInformation : personList){
                        if(personalInformation.getPin().equals(pin) && personalInformation.getDob().equals(dob)){

                        }else{
                            spotsDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Invalid User", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                        Log.e("isSuccessful : ", "Not Succesful");
                }
            }

            @Override
            public void onFailure(Call<UserLoginSuccessResponse> call, Throwable t) {
                Log.e("onFailure : ", "Servar Call Problem");
            }
        });
    }



}//End LoginActivity Class
