package com.example.user.brackdmccw.Activity.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.GetJson;
import com.example.user.brackdmccw.Activity.NetworkCall.APIUtils.ApiUtils;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.ProfileUpdatePojo.ProfileUpdateResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.AdminProfileUpdateApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.UserLoginSuccessApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.CommitteeDesignation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.District;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.IcsUdmt;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.PersonalInformation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.Upazilla;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.UserLoginSuccessResponse;
import com.example.user.brackdmccw.Activity.RealPathUtil;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.Activity.Utility;
import com.example.user.brackdmccw.R;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.squareup.picasso.Picasso;

import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class MemberProfileUpdateWithPhotoActivity extends AppCompatActivity {

    Button btn_back;
    Button btn_home;
    Button buttonHome;
    Button btnMenu;
    Button btnSave;
    Button btnCancel;
    Button btnRotate;

    ImageView ivPhotoUpload;
    EditText etPin;
    EditText etFirstName;
    EditText etLastName;
    EditText etProgramme;
    EditText etDesignation;
    EditText etCommitteeDesignation;
    Button etDateOfJoin;
    EditText etMobile;
    TextView tvImagePath;

    ArrayList<District>districtList = new ArrayList<District>();
    ArrayList<Upazilla>upazillaList = new ArrayList<Upazilla>();
    ArrayList<PersonalInformation>personalInformationList = new ArrayList<PersonalInformation>();
    ArrayList<IcsUdmt>icsUDMTList = new ArrayList<IcsUdmt>();
    ArrayList<CommitteeDesignation>committeeDesignationList = new ArrayList<CommitteeDesignation>();

    HashMap<String,String> profileUpdateMap = new HashMap<String, String>();

    private HashMap<String,String> personalInfromationMap = new HashMap<>();

    DatabaseHandler databaseHandler;

    private static int GALLAY_PICTURE_REQUEST = 1;
    private static final int CAMERA_PICTURE_REQUEST = 2;

    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;

    Bitmap actualBitmap = null;

    final int CAMERA_REQUEST = 13323;
    final int GALLERY_REQUEST = 22131;

    String photoPath = "";
    String pic_name_from_db = "";
    String pin = "";
    String first_name = "";
    String last_name = "";
    String program_name = "";
    String designation = "";
    String committee_designation = "";
    String doj = "";
    String mobile = "";
    String pic_name = "";

    Bitmap photoBitmap=null;

    static final int DIALOG_ID = 0;

    int year_x;
    int month_x;
    int day_x;

    AdminProfileUpdateApiService adminProfileUpdateApiService;
    UserLoginSuccessApiService userLoginSuccessApiService;
    UserLoginSuccessResponse userLoginSuccessResponse;
    ProgressDialog pDialog;
    String StringResponse = "";

    private String userChoosenTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_ics_udmt_members);


//******************** Hide Actionbar**************

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

//******************** Hide Actionbar**************

        adminProfileUpdateApiService = ApiUtils.getAdminProfileUpdateApiService();
        userLoginSuccessApiService = ApiUtils.getLoginSuccessAPIService();


//*********************** View Initialize ********************
        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        databaseHandler = new DatabaseHandler(this);

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_home = (Button) findViewById(R.id.btn_home);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        //btnRotate = (Button) findViewById(R.id.btnRotate);

        ivPhotoUpload = (ImageView) findViewById(R.id.ivPhotoUpload);
        etPin = (EditText) findViewById(R.id.etPin);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etProgramme = (EditText) findViewById(R.id.etProgramme);
        etDesignation = (EditText) findViewById(R.id.etDesignation);
        etCommitteeDesignation = (EditText) findViewById(R.id.etCommitteeDesignation);
        etDateOfJoin = (Button) findViewById(R.id.etDateOfJoin);
        etMobile = (EditText) findViewById(R.id.etMobile);
        tvImagePath = (TextView) findViewById(R.id.tvImagePath);


//***********************End View Initialize ********************

        //*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************




//***************** Get Information By personalInfo ID **************

//*********************** Select ICS Memeber Whoose Committee Type Is ICS*****************************

//        try {

        String chngeUpdQry = "";
        if(TempSaveData.FROM.equals(getIntent().getStringExtra("FROM_ICS_VIEW"))){
            chngeUpdQry = "SELECT dmcc_personal_information.*,dmcc_committee_designation.committee_designation FROM dmcc_personal_information " +
                    "LEFT JOIN dmcc_ics_udmt ON " +
                    "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                    "LEFT JOIN dmcc_committee_designation ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id " +
                    "WHERE dmcc_personal_information.id = "+"'"+TempSaveData.SelectIcsMemberViewPosition+"'"+" AND " +
                    "dmcc_ics_udmt.committee_type_id = 1 ";

            getPersonalInformationDataFromDb(chngeUpdQry);
            //TempSaveData.PersonalInformationID = TempSaveData.SelectIcsMemberViewPosition;
        }
        else if (TempSaveData.FROM.equals(getIntent().getStringExtra("FROM_UDMT_VIEW"))){
            chngeUpdQry = "SELECT dmcc_personal_information.*,dmcc_committee_designation.committee_designation FROM dmcc_personal_information " +
                    "LEFT JOIN dmcc_ics_udmt ON " +
                    "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                    "LEFT JOIN dmcc_committee_designation ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id " +
                    "WHERE dmcc_personal_information.id = "+"'"+TempSaveData.SelectUDMTMemberViewPosition+"'"+" AND " +
                    "dmcc_ics_udmt.committee_type_id = 2 ";
            getPersonalInformationDataFromDb(chngeUpdQry);
            //TempSaveData.PersonalInformationID = TempSaveData.SelectUDMTMemberViewPosition;
        }else{
            chngeUpdQry = "SELECT dmcc_personal_information.*,dmcc_committee_designation.committee_designation FROM dmcc_personal_information " +
                    "LEFT JOIN dmcc_ics_udmt ON " +
                    "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                    "LEFT JOIN dmcc_committee_designation ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id " +
                    "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"'";
            getPersonalInformationDataFromDb(chngeUpdQry);
        }


//        }catch (Exception ex){
//            Log.e("ProfileUpdateWithPhoto ERROR : ",ex.getMessage());
//        }

//*********************** Select ICS Memeber Whoose Member Type Is ICS *****************************




//***************** Get Information By personalInfo ID **************





//**************************** For Calender Current Date ***************************************************

        Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);

//**************************** For Calender Current Date ***************************************************
//*************************** InputDob Button Click Event Occur**********************************

        etDateOfJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  showDialog(DIALOG_ID);
            }
        });
//***************************End InputDob Button Click Event Occur**********************************

//**********************Back Button Click Event *********************

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getStringExtra("Activity").equals("SearchActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,SearchActivity.class);
                    startActivity(intent);
                    finish();
                }else if(getIntent().getStringExtra("Activity").equals("AdminICSFocalSearchActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,AdminICSFocalSearchActivity.class);
                    startActivity(intent);
                    finish();
                }else if(getIntent().getStringExtra("FROM_UDMT_VIEW").equals("UdmtMemberViewDetailsActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,UdmtMemberViewDetailsActivity.class);
                    startActivity(intent);
                    finish();
                }else if(getIntent().getStringExtra("FROM_ICS_VIEW").equals("IcsMemberViewDetailsActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,IcsMemberViewDetailsActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

//**********************End Back Button Click Event *********************

//**********************Home Button Click Event *********************

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });

//**********************End Home Button Click Event *********************

//**********************Cancel Button Click Event *********************

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TempSaveData.ScreenName.equals("SearchActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,SearchActivity.class);
                    startActivity(intent);
                    finish();
                }else if(TempSaveData.ScreenName.equals("ListOfIcsCommitteeMembersActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,ListOfIcsCommitteeMembersActivity.class);
                    startActivity(intent);
                    finish();
                }else if(TempSaveData.ScreenName.equals("ListOfUdmtMembersActivity")){
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,ListOfUdmtMembersActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,SearchActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

//**********************End Back Button Click Event *********************

//***********************For Popup Menu******************************
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(MemberProfileUpdateWithPhotoActivity.this,btnMenu);
                //Inflating the Popup using xml file

                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,AdminICSFocalSearchActivity
                                    .class);
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(MemberProfileUpdateWithPhotoActivity.this,MemberProfileUpdateWithPhotoActivity
                                    .class);
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){
                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");
                            Intent hsintent = new Intent(MemberProfileUpdateWithPhotoActivity.this,LoginActivity
                                    .class);
                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");
                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//***********************End Popup Menu******************************

//**********************Save Button Click Event *********************

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pin = etPin.getText().toString().trim();
                first_name = etFirstName.getText().toString().trim();
                last_name = etLastName.getText().toString().trim();
                program_name = etProgramme.getText().toString().trim();
                designation = etDesignation.getText().toString().trim();
                committee_designation = etCommitteeDesignation.getText().toString().trim();
                doj = etDateOfJoin.getText().toString().trim();
                mobile = etMobile.getText().toString().trim();
                pic_name = tvImagePath.getText().toString();

                if( !TextUtils.isEmpty(program_name) &&!TextUtils.isEmpty(designation) && !TextUtils.isEmpty(doj) && !TextUtils.isEmpty(mobile)){


                    profileUpdateMap.put("pin",pin);
                    profileUpdateMap.put("first_name",first_name);
                    profileUpdateMap.put("last_name",last_name);
                    profileUpdateMap.put("program_name",program_name);
                    profileUpdateMap.put("company_designation",designation);
                    //profileUpdateMap.put("committee_designation",committee_designation);
                    profileUpdateMap.put("committee_designation",committee_designation);
                    profileUpdateMap.put("doj",doj);
                    profileUpdateMap.put("mobile",mobile);
                    profileUpdateMap.put("pic_name",pic_name);

                    if(isNetworkConnected()){
                       //adminProfileUpdate(profileUpdateMap);
                        new ProfileUpdateRequest().execute();

                    }else{

                        Toast.makeText(MemberProfileUpdateWithPhotoActivity.this, "Please Check Your Network Connection",
                                Toast.LENGTH_SHORT).show();
                    }



                }
                else if(TextUtils.isEmpty(program_name)){
                    etProgramme.setError("Please enter program name");
                }
                else if(TextUtils.isEmpty(designation)){
                    etDesignation.setError("Please enter designation");
                }
                else if(TextUtils.isEmpty(doj)){
                    etDateOfJoin.setError("Please enter doj");
                }
                else if(TextUtils.isEmpty(mobile)){
                    etMobile.setError("Please enter mobile no");
                }
                else{
                    Toast.makeText(MemberProfileUpdateWithPhotoActivity.this, "Please input first", Toast
                            .LENGTH_SHORT).show();
                }

            }
        });

//**********************End Save Button Click Event *********************

//**********************Back Button Click Event *********************

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,
                        SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });

//**********************End Cancel Button Click Event *********************


//ImageView Click Event Occur Here
        ivPhotoUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent openImage = new Intent(
//
//                        Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//                startActivityForResult(openImage, RESULT_LOAD_IMAGE);

                choosePictureAction();

            }
        });
//End ImageView Click Event Occur Here

//        btnRotate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ivPhotoUpload.setRotation(ivPhotoUpload.getRotation()+(float)270.0);
//            }
//        });


    }//*******On Create Method End Here*********


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getPersonalInformationDataFromDb(String query) {


        int sl_no = 0;
        Log.e("SL No : ",String.valueOf(sl_no));
        Cursor cursorPersonalInfo = databaseHandler.rawQuery(query);
        if(cursorPersonalInfo!=null){
            if(cursorPersonalInfo.moveToFirst()){
                do{

                    sl_no++;

                    Log.e("SL No : ",String.valueOf(sl_no));

                    String first_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("first_name"));
                    etFirstName.setText(first_name);

                    String last_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("last_name"));
                    etLastName.setText(last_name);

//                        String id = String.valueOf(cursorPersonalInfo.getInt(cursorPersonalInfo.getColumnIndex
//                                ("id")));

                    String id = String.valueOf(cursorPersonalInfo.getInt(0));
                    TempSaveData.PersonalInformationID = id;

                    pic_name_from_db = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pic_name"));
                    //ivPhotoUpload.setImageBitmap(getBitmapFromURL(ApiUtils
                    //        .IMAGE_BASE_URL+pic_name_from_db));
                    try{
                        //ivPhotoUpload.setImageBitmap(getBitmapFromURL(ApiUtils
                       //         .IMAGE_BASE_URL+pic_name_from_db));

                    }catch (Exception ex){}


                        Picasso.with(MemberProfileUpdateWithPhotoActivity.this)
                                .load(ApiUtils.IMAGE_BASE_URL+pic_name_from_db)
                                .into(ivPhotoUpload);

                    String dob = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("dob"));

                    String doj = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("doj"));
                    etDateOfJoin.setText(dateFormatChange(doj));
                    String pin = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pin"));
                    etPin.setText(pin);
                    String program_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("program_name"));
                    etProgramme.setText(program_name);
                    String committee_designation = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("committee_designation"));
                    etCommitteeDesignation.setText(committee_designation);
                    String company_designation = cursorPersonalInfo.getString(cursorPersonalInfo
                            .getColumnIndex("company_designation"));
                    etDesignation.setText(company_designation);
                    String phone = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("phone"));
                    String mobile = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("mobile"));
                    etMobile.setText(mobile);


                }while(cursorPersonalInfo.moveToNext());
            }

        }

    }

    //********************* Start adminProfileUpdate Method ******************
//    private void adminProfileUpdate(final HashMap<String, String> profileUpdateMap) {
//        JSONObject paramObject = new JSONObject();
//        try {
//
//            paramObject.put("id",TempSaveData.PersonalInformationID);
//            paramObject.put("program_name", profileUpdateMap.get("program_name"));
//            paramObject.put("company_designation", profileUpdateMap.get("company_designation"));
//            paramObject.put("mobile", profileUpdateMap.get("mobile"));
//
//
//            try {
//                if (profileUpdateMap.get("pic_name").equals("")){
//                    photoBitmap = getImageBitmapFromUrl(ApiUtils.IMAGE_BASE_URL+pic_name_from_db);
////                    Bitmap resizedPhotoBitmap = Bitmap.createScaledBitmap(photoBitmap,300,300,
//                      //      true);
//                }else{
//                    photoBitmap = ImageLoader.init().from(profileUpdateMap.get("pic_name")).getBitmap();
//                  //  Bitmap resizedPhotoBitmap = Bitmap.createScaledBitmap(photoBitmap,300,300,
//                 //           true);
//                   // String imageBase64toString = ImageBase64.encode(actualBitmap);
//                    String imageBase64toString = imageToString(actualBitmap);
//                    try {
//                        paramObject.put("pic_name",imageBase64toString);
//
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    //paramObject.put("pic_name","data:image/png;base64,"+imageString);
//                   // paramObject.put("pic_name","data:image/png;base64,"+imageBase64toString);
//
//                }
//
//
//                //Log.e("ImageBase64ToString : ",imageBase64toString);
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//
//            Log.e("Request JSON For Profile Update : ",paramObject.toString());
//            adminProfileUpdateApiService.requestForProfileUpdate(paramObject.toString()).enqueue
//                    (new Callback<ProfileUpdateResponse>() {
//                        @Override
//                        public void onResponse(Call<ProfileUpdateResponse> call, Response<ProfileUpdateResponse> response) {
//                            if (response.isSuccessful()){
//
//                                final ProfileUpdateResponse profileUpdateResponse = response.body();
//
////                                Toast.makeText(MemberProfileUpdateWithPhotoActivity.this,
////                                        profileUpdateResponse.getMessage(), Toast
////                                                .LENGTH_SHORT).show();
//                                if (profileUpdateResponse.getStatus().equals("success")){
//
//                                    userLoginSuccessApiService.requestForGetInformation().enqueue
//                                            (new Callback<UserLoginSuccessResponse>() {
//                                                @Override
//                                                public void onResponse(Call<UserLoginSuccessResponse> call, Response<UserLoginSuccessResponse> response) {
//
//                                                    userLoginSuccessResponse = response.body();
//
//                                                    personalInformationList = (ArrayList<PersonalInformation>) userLoginSuccessResponse.getPersonalInformation();
//
//
//                                                    for (PersonalInformation personalInformation : personalInformationList) {
//
//                                                        personalInfromationMap.clear();
//
//                                                        Log.e("Personal Info List",personalInformation.getFirstName());
//
//                                                        personalInfromationMap.put("id",personalInformation.getId());
//                                                        personalInfromationMap.put("first_name",
//                                                                personalInformation.getFirstName());
//                                                        personalInfromationMap.put("last_name",
//                                                                personalInformation.getLastName());
//                                                        personalInfromationMap.put("pic_name",
//                                                                personalInformation.getPicName());
//                                                        personalInfromationMap.put("dob",personalInformation.getDob());
//                                                        personalInfromationMap.put("doj",personalInformation
//                                                                .getDoj());
//                                                        personalInfromationMap.put("pin",personalInformation
//                                                                .getPin());
//                                                        personalInfromationMap.put("program_name",
//                                                                personalInformation.getProgramName());
//                                                        personalInfromationMap.put("company_designation",
//                                                                personalInformation.getCompanyDesignation());
//                                                        personalInfromationMap.put("phone",
//                                                                personalInformation.getPhone());
//                                                        personalInfromationMap.put("mobile",
//                                                                personalInformation.getMobile());
//                                                        personalInfromationMap.put("current_work_station",
//                                                                personalInformation.getCurrentWorkStation());
//                                                        personalInfromationMap.put("created_by",
//                                                                personalInformation.getCreatedBy());
//                                                        personalInfromationMap.put("updated_by",
//                                                                personalInformation.getUpdatedBy());
//                                                        personalInfromationMap.put("created_at",
//                                                                personalInformation.getCreatedAt());
//                                                        personalInfromationMap.put("updated_at",
//                                                                personalInformation.getUpdatedAt());
//
//
//
////                                                        if(!databaseHandler.checkDuplicate("dmcc_personal_information", "id", personalInformation.getId()))
////                                                            databaseHandler.InsertTable(personalInfromationMap,"dmcc_personal_information");
////                                                        else
//                                                            databaseHandler.UpdateTable(personalInfromationMap, "dmcc_personal_information", "id", personalInformation.getId());
//
////                                                          databaseHandler.excQuery("UPDATE dmcc_personal_information SET program_name ='', " +
////                                                                    "company_designation = 'false',mobile ='',pic_name ='', WHERE " +
////                                                                    "id = "+"'"+TempSaveData.SelectIcsMemberViewPosition+"'");
//
//                                                        Toast.makeText
//                                                                (MemberProfileUpdateWithPhotoActivity.this,profileUpdateResponse.getMessage(), Toast.LENGTH_SHORT).show();
//
//                                                        if(TempSaveData.FROM.equals("IcsMemberViewDetailsActivity")){
//                                                            Intent hsintent = new Intent(MemberProfileUpdateWithPhotoActivity.this,ListOfIcsCommitteeMembersAddNewActivity
//                                                                    .class);
//                                                            startActivity(hsintent);
//                                                            finish();
//                                                        }else if(TempSaveData.FROM.equals
//                                                                ("UdmtMemberViewDetailsActivity")){
//                                                            Intent hsintent = new Intent
//                                                                    (MemberProfileUpdateWithPhotoActivity.this,ListOfUdmtMemberAddNewActivity
//                                                                            .class);
//                                                            startActivity(hsintent);
//                                                            finish();
//                                                        }else{
//                                                            Intent hsintent = new Intent
//                                                                    (MemberProfileUpdateWithPhotoActivity.this,SearchActivity
//                                                                            .class);
//                                                            startActivity(hsintent);
//                                                            finish();
//                                                        }
//
//
//                                                        Log.e("personal info table insert",personalInfromationMap.toString());
//                                                    }
//
//                                                }
//
//                                                @Override
//                                                public void onFailure(Call<UserLoginSuccessResponse> call, Throwable t) {
//                                                    Toast.makeText
//                                                            (MemberProfileUpdateWithPhotoActivity
//                                                                    .this, "Something " +
//                                                                            "Wrong!!!!!!!",
//                                                                    Toast.LENGTH_SHORT).show();
//                                                }
//                                            });
//                                }
//
//
//                            }
//
//                        }
//
//                        @Override
//                        public void onFailure(Call<ProfileUpdateResponse> call, Throwable t) {
//                            Log.e("Api Call Failure Method",t.toString());
//                        }
//                    });
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//********************* End adminProfileUpdate Method ******************



    //**********************************For DatePicker dialog**********************************************
    @Override
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_ID){
            DatePickerDialog datePickerDialog =  new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
            return datePickerDialog;
        }
        else{
            return null;
        }

    }
    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            year_x = year;
            // month_x = month + 1;
            month_x = month;
            day_x = day;

            //etDepositDate.setText(day_x+"-"+month_x+"-"+year_x);
            etDateOfJoin.setText(formatDate(year_x,month_x,day_x));
        }
    };

    private static String formatDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(date);
    }
    //**********************************End DatePicker dialog**********************************************


//**************************** Photo Result From Gallary or Camera ************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLAY_PICTURE_REQUEST && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String imagePath=getRealPathFromURI(selectedImage);
            try {
                Bitmap bitmap = ImageLoader.init().from(imagePath).requestSize(300, 300)
                        .getBitmap();
                //ivPhotoUpload.setImageBitmap(bitmap);
    //************* Eliminate Image Rotate Change When You Capture Image From Camera Request ***********
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(imagePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = bitmap;
                }
     //************* Eliminate Image Rotate Change When You Capture Image From Camera Request ***********
                ivPhotoUpload.setImageBitmap(rotatedBitmap);
                actualBitmap = rotatedBitmap;


            }catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //Drawable d = Drawable.createFromPath(imagePath);
           // ivPhotoUpload.setImageDrawable(d);
            tvImagePath.setVisibility(View.INVISIBLE);
            if (!imagePath.equals("")){
                tvImagePath.setText(imagePath);
            }



          //  String stringUri = selectedImageUri.getPath();
         //   galImageView.setImageURI(null);
         //  galImageView.setImageURI(Uri.parse(stringUri));

        }else if((requestCode == CAMERA_PICTURE_REQUEST) && (resultCode == RESULT_OK) && (data !=
                null)){

            Uri imageUriPath = data.getData();

            //From Dada
            actualBitmap = (Bitmap) data.getExtras().get("data");//work for lenovo , symphony, walton , oppo
            ivPhotoUpload.setImageBitmap(actualBitmap);//work for lenovo , symphony, walton , oppo
            //Dada



            //Bitmap photo = (Bitmap) data.getExtras().get("data");

           // String imagePath=getRealPathFromImageURI(selectedImage);

            /*
            try {
                Bitmap bitmap = ImageLoader.init().from(imagePath).requestSize(300, 300)
                        .getBitmap();
                //ivPhotoUpload.setImageBitmap(bitmap);
    //************* Eliminate Image Rotate Change When You Capture Image From Camera Request ***********
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(imagePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = bitmap;
                }
    //************* Eliminate Image Rotate Change When You Capture Image From Camera Request ***********
                ivPhotoUpload.setImageBitmap(rotatedBitmap);
                actualBitmap = rotatedBitmap;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            */

            //Drawable d = Drawable.createFromPath(imagePath);
            //ivPhotoUpload.setImageDrawable(d);
            tvImagePath.setVisibility(View.INVISIBLE);
            try{
                if (!actualBitmap.toString().equals("")){
                    tvImagePath.setText(actualBitmap.toString());//work for lenovo , symphony, walton , oppo
                }
            }catch (Exception e){
                Log.e("CameraException : ",e.getMessage());
                String imagePath=getRealPathFromURI(imageUriPath);
                Bitmap bitmap = null;
                Bitmap rotateBitmap = null;
                try {
                    bitmap = ImageLoader.init().from(imagePath).requestSize(300, 300)
                            .getBitmap();
                    rotateBitmap = rotateBitmapOrientation(imagePath);
                    rotateBitmap = getResizedBitmap(rotateBitmap,300,300);
                    actualBitmap = rotateBitmap;
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                //ivPhotoUpload.setImageBitmap(bitmap);
                ivPhotoUpload.setImageBitmap(rotateBitmap);
                if (!imagePath.equals("")){
                    tvImagePath.setText(imagePath);//work for samsung
                }

            }


        }



        //Use For PhotoUtil Library
/*
        if(resultCode == RESULT_OK){
            if(requestCode == CAMERA_REQUEST){
                photoPath = cameraPhoto.getPhotoPath();
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
                    ivPhotoUpload.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(),
                            "Something Wrong while loading photos", Toast.LENGTH_SHORT).show();
                }

            }
            else if(requestCode == GALLERY_REQUEST){

                Uri uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                photoPath = galleryPhoto.getPath();

                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
                    ivPhotoUpload.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(),
                            "Something Wrong while choosing photos", Toast.LENGTH_SHORT).show();
                }
            }
        }
        */
        //Use For PhotoUtil Library






    }//End OnActivityResult Method
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
    private Bitmap rotateBitmapOrientation(String imagePath) {
        // Create and configure BitmapFactory
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bounds);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(imagePath, opts);
        // Read EXIF Data
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
        // Rotate Bitmap
        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        // Return result
        return rotatedBitmap;

    }


    //Get Image Path Location From Uri
    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    /*
    * Get Real Path
    */
    private String getRealPathFromImageURI(Uri contentUri) {
        String result;
        final String[] projection = new String[]{"*"};
        Cursor cursor = getApplicationContext().getContentResolver().query(contentUri, projection, null, null, null);
        if (cursor == null) {
            result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int indx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(indx);
            cursor.close();
        }
        return result;
    }

//**************************** End Photo Result From Gallary or Camera ************************
    public Bitmap eliminateRotateProblemImage(Bitmap bitmap,String imagePath){
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


//**************************** Choose Picture Option From Dialogue ************************
    private void choosePictureAction() {

        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app
                .AlertDialog.Builder(MemberProfileUpdateWithPhotoActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean result=Utility.checkPermission(MemberProfileUpdateWithPhotoActivity.this);
                if (items[which].equals("Camera")) {
                    userChoosenTask="Camera";
                    if(result)
                        cameraIntent();
                    //Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    //startActivityForResult(intent, CAMERA_PICTURE_REQUEST);

                    //Use For PhotoUtil Library

//                    try {
//                        startActivityForResult(cameraPhoto.takePhotoIntent(), CAMERA_REQUEST);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    cameraPhoto.addToGallery();

                    //End Use For PhotoUtil Library

                } else if (items[which].equals("Gallery")) {
                    userChoosenTask="Choose from Library";
                    if(result)
                        galleryIntent();
                    //Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore
                    //        .Images.Media.EXTERNAL_CONTENT_URI);
                    //startActivityForResult(intent, GALLAY_PICTURE_REQUEST);

                    //Use For PhotoUtil Library
                    //startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
                    //Use For PhotoUtil Library

                } else if (items[which].equals("Cancel")) {

                    dialog.dismiss();

                }

            }
        });

        builder.show();

    }
//**************************** End Choose Picture Option From Dialogue ************************
 //****** Date fromat Change(2017-12-15 to 15-12-2017) ********
public String dateFormatChange(String date){

    //String ds1 = "2007-06-30";
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
    String ds2 = null;
    try {
        ds2 = sdf2.format(sdf1.parse(date));
    } catch (ParseException e) {
        e.printStackTrace();
    }
    System.out.println(ds2); //will be 30/06/2007

    return ds2;
}
    //****** Date fromat Change ********

    public static Bitmap getBitmapFromURL(String imgUrl) {
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static Bitmap getImageBitmapFromUrl(String imgPath)
    {
        Bitmap bm = null;
        try {
            URL url = new URL(imgPath);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            if(conn.getResponseCode() != 200)
            {
                return bm;
            }
            conn.connect();
            InputStream is = conn.getInputStream();

            BufferedInputStream bis = new BufferedInputStream(is);
            try
            {
                bm = BitmapFactory.decodeStream(bis);
            }
            catch(OutOfMemoryError ex)
            {
                bm = null;
            }
            bis.close();
            is.close();
        } catch (Exception e) {}

        return bm;
    }


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }
    private String imageToString (Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgTobByteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgTobByteArray,Base64.DEFAULT);
    }

    private void selectImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,CAMERA_PICTURE_REQUEST);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
//            Intent idd = new Intent(MemberProfileUpdateWithPhotoActivity.this, SearchActivity.class);
//            startActivity(idd);
//            finish();
//            return true;
            if(TempSaveData.ScreenName.equals("SearchActivity")){
                Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,SearchActivity.class);
                startActivity(intent);
                finish();
                //return true;
            }else if(TempSaveData.ScreenName.equals("AdminICSFocalSearchActivity")){
                Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,AdminICSFocalSearchActivity.class);
                startActivity(intent);
                finish();
                //return true;
            }else if(TempSaveData.FROM.equals("UdmtMemberViewDetailsActivity")){
                Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,UdmtMemberViewDetailsActivity.class);
                startActivity(intent);
                finish();
                //return true;
            }else if(TempSaveData.FROM.equals("IcsMemberViewDetailsActivity")){
                Intent intent = new Intent(MemberProfileUpdateWithPhotoActivity.this,IcsMemberViewDetailsActivity.class);
                startActivity(intent);
                finish();
                //return true;
            }

        }
        return super.onKeyDown(keyCode, event);

    }



    private class ProfileUpdateRequest extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MemberProfileUpdateWithPhotoActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }



        @Override
        protected String doInBackground(String... args) {

            ArrayList<String> key=new ArrayList<String>();
            ArrayList<String> value=new ArrayList<String>();

            if(!pic_name.equals("")){
                key.add("id");
                key.add("program_name");
                key.add("company_designation");
                key.add("mobile");
                key.add("pic_name");

                value.add(TempSaveData.PersonalInformationID);
                value.add(program_name);
                value.add(designation);
                value.add(mobile);
                value.add(imageToString(actualBitmap));
            }else{
                key.add("id");
                key.add("program_name");
                key.add("company_designation");
                key.add("mobile");
                key.add("pic_name");

                value.add(TempSaveData.PersonalInformationID);
                value.add(program_name);
                value.add(designation);
                value.add(mobile);
                value.add("");
            }

            StringResponse= GetJson.fetchJSON(ApiUtils.BASE_URL+"set_profile_updated_data", key, value);

            Log.e("StringResponse", StringResponse);
            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            runOnUiThread(new Runnable() {
                @SuppressLint("SetJavaScriptEnabled")
                @Override
                public void run() {

                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(StringResponse);
                        //JSONArray jArray = jsonObj.getJSONArray("retailer");
                        //JSONObject jsonData = jArray.getJSONObject(0);
                        if(jsonObj.getString("status").equalsIgnoreCase("success"))
                        {
                            /* Dada
                            databaseHandler.UpdateTable(profileUpdateMap,"dmcc_personal_information", "id", TempSaveData.PersonalInformationID);
                            Toast.makeText(getApplicationContext(), jsonObj.getString("message"), Toast.LENGTH_SHORT).show();
                            */

//
//                            if(TempSaveData.FROM.equalsIgnoreCase("IcsMemberViewDetailsActivity")){
//                                Intent hsintent = new Intent(MemberProfileUpdateWithPhotoActivity.this,ListOfIcsCommitteeMembersAddNewActivity
//                                        .class);
//                                startActivity(hsintent);
//                                finish();
//                            }else if(TempSaveData.FROM.equalsIgnoreCase
//                                    ("UdmtMemberViewDetailsActivity")){
//                                Intent hsintent = new Intent
//                                        (MemberProfileUpdateWithPhotoActivity.this,ListOfUdmtMemberAddNewActivity
//                                                .class);
//                                startActivity(hsintent);
//                                finish();
//                            }else if(TempSaveData.ScreenName.equalsIgnoreCase
//                                    ("ListOfIcsCommitteeMembersAddNewActivity")){
//                                Intent hsintent = new Intent
//                                        (MemberProfileUpdateWithPhotoActivity.this,ListOfIcsCommitteeMembersAddNewActivity
//                                                .class);
//                                startActivity(hsintent);
//                                finish();
//                            }else if(TempSaveData.ScreenName.equalsIgnoreCase
//                                    ("ListOfUdmtMemberAddNewActivity")){
//                                Intent hsintent = new Intent
//                                        (MemberProfileUpdateWithPhotoActivity
//                                                .this,AdminICSFocalSearchActivity
//                                                .class);
//                                startActivity(hsintent);
//                                finish();
//                            }else{
//                                Intent hsintent = new Intent
//                                        (MemberProfileUpdateWithPhotoActivity.this,SearchActivity
//                                                .class);
//                                startActivity(hsintent);
//                                finish();
//                            }


                            userLoginSuccessApiService.requestForGetInformation().enqueue
                                    (new Callback<UserLoginSuccessResponse>() {
                                        @Override
                                        public void onResponse(Call<UserLoginSuccessResponse> call, Response<UserLoginSuccessResponse> response) {

                                            userLoginSuccessResponse = response.body();

                                            personalInformationList = (ArrayList<PersonalInformation>) userLoginSuccessResponse.getPersonalInformation();


                                            for (PersonalInformation personalInformation : personalInformationList) {

                                                personalInfromationMap.clear();

                                                Log.e("Personal Info List",personalInformation.getFirstName());

                                                personalInfromationMap.put("id",personalInformation.getId());
                                                personalInfromationMap.put("first_name",
                                                        personalInformation.getFirstName());
                                                personalInfromationMap.put("last_name",
                                                        personalInformation.getLastName());
                                                personalInfromationMap.put("pic_name",
                                                        personalInformation.getPicName());
                                                personalInfromationMap.put("dob",personalInformation.getDob());
                                                personalInfromationMap.put("doj",personalInformation
                                                        .getDoj());
                                                personalInfromationMap.put("pin",personalInformation
                                                        .getPin());
                                                personalInfromationMap.put("program_name",
                                                        personalInformation.getProgramName());
                                                personalInfromationMap.put("company_designation",
                                                        personalInformation.getCompanyDesignation());
                                                personalInfromationMap.put("phone",
                                                        personalInformation.getPhone());
                                                personalInfromationMap.put("mobile",
                                                        personalInformation.getMobile());
                                                personalInfromationMap.put("current_work_station",
                                                        personalInformation.getCurrentWorkStation());
                                                personalInfromationMap.put("created_by",
                                                        personalInformation.getCreatedBy());
                                                personalInfromationMap.put("updated_by",
                                                        personalInformation.getUpdatedBy());
                                                personalInfromationMap.put("created_at",
                                                        personalInformation.getCreatedAt());
                                                personalInfromationMap.put("updated_at",
                                                        personalInformation.getUpdatedAt());



//                                                        if(!databaseHandler.checkDuplicate("dmcc_personal_information", "id", personalInformation.getId()))
//                                                            databaseHandler.InsertTable(personalInfromationMap,"dmcc_personal_information");
//                                                        else
                                                databaseHandler.UpdateTable(personalInfromationMap, "dmcc_personal_information", "id", personalInformation.getId());

//                                                          databaseHandler.excQuery("UPDATE dmcc_personal_information SET program_name ='', " +
//                                                                    "company_designation = 'false',mobile ='',pic_name ='', WHERE " +
//                                                                    "id = "+"'"+TempSaveData.SelectIcsMemberViewPosition+"'");



                                                Log.e("personal info table insert",personalInfromationMap.toString());
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<UserLoginSuccessResponse> call, Throwable t) {
                                            Toast.makeText
                                                    (MemberProfileUpdateWithPhotoActivity
                                                                    .this, "Something " +
                                                                    "Wrong!!!!!!!",
                                                            Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            if(TempSaveData.FROM.equals("IcsMemberViewDetailsActivity")){
                                Intent hsintent = new Intent(MemberProfileUpdateWithPhotoActivity.this,ListOfIcsCommitteeMembersAddNewActivity
                                        .class);
                                startActivity(hsintent);
                                finish();
                            }else if(TempSaveData.FROM.equals
                                    ("UdmtMemberViewDetailsActivity")){
                                Intent hsintent = new Intent
                                        (MemberProfileUpdateWithPhotoActivity.this,ListOfUdmtMemberAddNewActivity
                                                .class);
                                startActivity(hsintent);
                                finish();
                            }else if(TempSaveData.ScreenName.equals
                                    ("ListOfIcsCommitteeMembersAddNewActivity")){
                                Intent hsintent = new Intent
                                        (MemberProfileUpdateWithPhotoActivity.this,ListOfIcsCommitteeMembersAddNewActivity
                                                .class);
                                startActivity(hsintent);
                                finish();
                            }else if(TempSaveData.ScreenName.equals
                                    ("ListOfUdmtMemberAddNewActivity")){
                                Intent hsintent = new Intent
                                        (MemberProfileUpdateWithPhotoActivity
                                                .this,AdminICSFocalSearchActivity
                                                .class);
                                startActivity(hsintent);
                                finish();
                            }else{
                                Intent hsintent = new Intent
                                        (MemberProfileUpdateWithPhotoActivity.this,SearchActivity
                                                .class);
                                startActivity(hsintent);
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), jsonObj.getString("message"),Toast.LENGTH_SHORT).show();


                        }
                        else
                            Toast.makeText(getApplicationContext(), jsonObj.getString("message"), Toast.LENGTH_SHORT).show();


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });


            pDialog.dismiss();

        }

    }


    public String bitmpTo64(Bitmap photo)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Camera"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Gallery"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLAY_PICTURE_REQUEST);
    }

    private void cameraIntent() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_PICTURE_REQUEST);
    }


}
