package com.example.user.brackdmccw.Activity.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {


    Button btn_back;
    Button btn_home;
    Button buttonHome;
    Button btnMenu;

    Spinner spDistrict;
    Spinner spUpazilla;
    Button btnSearch;

    ArrayList<String> districtNameList;
    ArrayList<String> districtIDlist;

    ArrayList<String> upazillzList;
    ArrayList<String> upazillaIdList;

    DatabaseHandler databaseHandler;

    String districtIdSelectFromSpinner = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();

        databaseHandler = new DatabaseHandler(this);

        //View Initialize Here

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_home = (Button) findViewById(R.id.btn_home);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btnMenu = (Button) findViewById(R.id.btnMenu);

        spDistrict = (Spinner) findViewById(R.id.spDistrict);
        spUpazilla = (Spinner) findViewById(R.id.spUpazilla);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        //End View Initialize Here

        //Set District List Dropdown Here
        districtNameList = new ArrayList<>();
        districtIDlist = new ArrayList<>();
        upazillaIdList = new ArrayList<>();
        upazillzList = new ArrayList<>();
        upazillzList.add("Select Upazilla");


//*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************


        parseDistrict();

        //End Set District List Dropdown Here

        //Set Upazilla List Dropdown Here

        /*

        upazillzList = new ArrayList<>();
        upazillzList.add("Select Upazilla");

        Cursor cursorUpazilla = databaseHandler.rawQuery("SELECT * FROM upazilla");

        if(cursorUpazilla!=null)
        {
            if(cursorUpazilla.moveToFirst())
            {

                do{

                    //HashMap<String,String> map=new HashMap<String,String>();
                    //map.put("id", String.valueOf(cursor.getInt(cursor.getColumnIndex("id"))));
                    // map.put("division_id", cursor.getString(cursor.getColumnIndex
                    // ("division_id")));
                    //map.put("district_name", cursor.getString(cursor.getColumnIndex
                    //        ("division_id")));
                    upazillzList.add(cursorUpazilla.getString(cursorUpazilla.getColumnIndex("upazilla_name")));

                }
                while (cursorUpazilla.moveToNext());
            }
        }

        ArrayAdapter<String> upazillzListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,upazillzList);
        upazillzListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUpazilla.setAdapter(upazillzListAdapter);

        */

        //End Set Upazilla List Dropdown Here

 //**************** Parse Upazilla By District Select ****************************

        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                districtIdSelectFromSpinner = districtIDlist.get(arg2);
                TempSaveData.SelectDistrictId = districtIDlist.get(arg2);
                //String _ProductCategoryName = ProductCategoryName.get(arg2);
                ParseUpazillaByDistrictId(districtIdSelectFromSpinner);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });



 //**************** End Parse Upazilla By District Select *************************

buttonHome.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(SearchActivity.this, SearchActivity.class);
        startActivity(intent);
        finish();
    }
});


        //Search button event occur here and two of theme are select then it goes to ListOfUdmtMembersActivity
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((spDistrict.getSelectedItem().toString().trim() == "Select District")&&(spUpazilla.getSelectedItem().toString().trim() == "Select Upazilla")){

                    Toast.makeText(SearchActivity.this, "Please input first", Toast.LENGTH_SHORT).show();
                }
                else{

                    if((spDistrict.getSelectedItem().toString().trim() != "Select District")&&(spUpazilla.getSelectedItem().toString().trim() == "Select Upazilla")) {

                        TempSaveData.SelectDistrictName = spDistrict.getSelectedItem().toString().trim();

                        Intent intent = new Intent(SearchActivity.this, ListOfIcsCommitteeMembersActivity.class);
                        intent.putExtra("Activity","SearchActivity");
                        startActivity(intent);
                        finish();

                    }else{

                        TempSaveData.SelectDistrictName = spDistrict.getSelectedItem().toString().trim();
                        TempSaveData.SelectUpazillatName = spUpazilla.getSelectedItem().toString().trim();

                        Intent intent = new Intent(SearchActivity.this, ListOfUdmtMembersActivity.class);
                        intent.putExtra("Activity","SearchActivity");
                        startActivity(intent);
                        finish();
                    }

                }

            }
        });

//**************************** For Menu Item Work ***************************************************
        //********** Menu Check Result **********
        String menuCheckQry = "";
        //********** Menu Check Result **********

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(SearchActivity.this,btnMenu);
                //Inflating the Popup using xml file

                //MemberTypeId 1 = Admin
                //MemberTypeId 2 = ICS/UDMT Member
                //Otherwise =  General User
                if(TempSaveData.MemberTypeId.equals("1")){
                    popup.getMenuInflater().inflate(R.menu.ics_focal_menu, popup.getMenu());
                }else if (TempSaveData.MemberTypeId.equals("2")){
                    popup.getMenuInflater().inflate(R.menu.ics_udmt_members_menu, popup.getMenu());
                }else{
                    popup.getMenuInflater().inflate(R.menu.general_user_menu, popup.getMenu());
                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();

                        if(item.getItemId() == R.id.adminIcsFocal){
                            Intent intent = new Intent(SearchActivity.this,AdminICSFocalSearchActivity.class);
                            intent.putExtra("Activity","SearchActivity");
                            TempSaveData.ScreenName = "SearchActivity";
                            startActivity(intent);
                            finish();
                        }else if (item.getItemId() == R.id.profileUpdate){

                            Intent hsintent = new Intent(SearchActivity.this,MemberProfileUpdateWithPhotoActivity.class);
                            hsintent.putExtra("Activity","SearchActivity");
                            TempSaveData.ScreenName = "SearchActivity";
                            startActivity(hsintent);
                            finish();

                        }else if (item.getItemId() == R.id.logout){

                            databaseHandler.deleteAllRow("dmcc_personal_information");
                            databaseHandler.deleteAllRow("dmcc_ics_udmt");
                            databaseHandler.deleteAllRow("dmcc_committee_designation");

                            savePreference("pin","NO PREFERENCE");
                            savePreference("dob","NO PREFERENCE");

                            Intent hsintent = new Intent(SearchActivity.this,LoginActivity.class);
                            hsintent.putExtra("Activity","SearchActivity");
                            startActivity(hsintent);
                            finish();

                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
//**********************************End Menu Item Work**********************************************


    }//*******On Create Method End Here*********

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    SearchActivity.this);

            // set dialog message
            alertDialogBuilder
                    .setMessage("Do you want to Exit it?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

//                        databaseHandler.deleteAllRow("dmcc_personal_information");
//                        databaseHandler.deleteAllRow("dmcc_ics_udmt");
//                        databaseHandler.deleteAllRow("dmcc_committee_designation");

//                        savePreference("pin","NO PREFERENCE");
//                        savePreference("dob","NO PREFERENCE");

                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent
                                    .FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                            finish();

                        }
                    })
                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }

    private void parseDistrict() {
        districtNameList.add("Select District");
        districtIDlist.add("00");

        Cursor cursorDistrict = databaseHandler.rawQuery("SELECT * FROM district");

        if(cursorDistrict!=null)
        {
            if(cursorDistrict.moveToFirst())
            {

                do{
                    districtIDlist.add(String.valueOf(cursorDistrict.getInt(cursorDistrict.getColumnIndex("id"))));
                    districtNameList.add(cursorDistrict.getString(cursorDistrict.getColumnIndex("district_name")));
                }
                while (cursorDistrict.moveToNext());
            }
        }



        ArrayAdapter<String> districtListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,districtNameList);
        districtListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDistrict.setAdapter(districtListAdapter);
    }

    private void ParseUpazillaByDistrictId(String districtIdSelectFromSpinner) {

        upazillzList.clear();
        upazillaIdList.clear();
        upazillzList.add("Select Upazilla");
        upazillaIdList.add("00");
        Cursor cursorUpazilla = databaseHandler.rawQuery("SELECT * FROM upazilla WHERE district_id = "+"'"+districtIdSelectFromSpinner+"'");

        if(cursorUpazilla!=null)
        {
            if(cursorUpazilla.moveToFirst())
            {

                do{

                    //HashMap<String,String> map=new HashMap<String,String>();
                    //map.put("id", String.valueOf(cursor.getInt(cursor.getColumnIndex("id"))));
                    // map.put("division_id", cursor.getString(cursor.getColumnIndex
                    // ("division_id")));
                    //map.put("district_name", cursor.getString(cursor.getColumnIndex
                    //        ("division_id")));
                    upazillaIdList.add(String.valueOf(cursorUpazilla.getInt(cursorUpazilla.getColumnIndex("id"))));
                    upazillzList.add(cursorUpazilla.getString(cursorUpazilla.getColumnIndex("upazilla_name")));

                }
                while (cursorUpazilla.moveToNext());
            }
        }

        try{
            ArrayAdapter<String> upazillzListAdapter = new ArrayAdapter<String>(this,R.layout.spinner_text,upazillzList);
            upazillzListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spUpazilla.setAdapter(upazillzListAdapter);
            spUpazilla.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    TempSaveData.SelectUpazillatId = upazillaIdList.get(position);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch (Exception ex){

        }

    }

}
