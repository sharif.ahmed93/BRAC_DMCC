package com.example.user.brackdmccw.Activity.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.NetworkCall.APIUtils.ApiUtils;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class UdmtMemberViewDetailsActivity extends AppCompatActivity {



    Button btn_back;
    Button btn_home;
    Button buttonHome;
    Button btnMenu;
    Button btnEdit;
    Button btnCancel;
    Button btnRotate;

    ImageView ivPhotoUpload;
    TextView tvFirst;
    TextView tvLast;
    TextView etPin;
    TextView etFirstName;
    TextView etLastName;
    TextView etProgramme;
    TextView etDesignation;
    TextView etCommitteeDesignation;
    TextView etDateOfJoin;
    TextView etMobile;

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_udmt_member_view_details);


        getSupportActionBar().hide();

        databaseHandler = new DatabaseHandler(this);


//*********************** View Initialize ********************

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_home = (Button) findViewById(R.id.btn_home);
        buttonHome = (Button) findViewById(R.id.buttonHome);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        btnMenu.setVisibility(View.INVISIBLE);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        ivPhotoUpload = (ImageView) findViewById(R.id.ivPhotoUpload);
        tvFirst = (TextView) findViewById(R.id.tvFirst);
        etPin = (TextView) findViewById(R.id.etPin);
        etFirstName = (TextView) findViewById(R.id.etFirstName);
        etLastName = (TextView) findViewById(R.id.etLastName);
        etProgramme = (TextView) findViewById(R.id.etProgramme);
        etDesignation = (TextView) findViewById(R.id.etDesignation);
        etCommitteeDesignation = (TextView) findViewById(R.id.etCommitteeDesignation);
        etDateOfJoin = (TextView) findViewById(R.id.etDateOfJoin);
        etMobile = (TextView) findViewById(R.id.etMobile);


//***********************End View Initialize ********************

        //*************Check Admin Or Member By Login Pin*************
        String checkAdminQry = "SELECT dmcc_ics_udmt.member_type_id FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
        Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);
        if(adminCheckCursor != null){
            if(adminCheckCursor.moveToFirst()){
                do{
                    String member_type_id = String.valueOf(adminCheckCursor.getInt(0));
                    TempSaveData.MemberTypeId = member_type_id;
                }while (adminCheckCursor.moveToNext());
            }
        }
//*************End Check Admin Or Member By Login Pin*************




//*********************** Select UDMT Memeber Whoose Committee Type Is
// UDMT*****************************

    try {


        String chngeUpdQry = "SELECT dmcc_personal_information.*,dmcc_committee_designation.committee_designation FROM dmcc_personal_information " +
                "LEFT JOIN dmcc_ics_udmt ON " +
                "dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                "LEFT JOIN dmcc_committee_designation ON dmcc_committee_designation.id = dmcc_ics_udmt.committee_designation_id " +
                "WHERE dmcc_personal_information.id = "+"'"+TempSaveData.SelectUDMTMemberViewPosition+"'"+" AND " +
                "dmcc_ics_udmt.committee_type_id = 2";


        int sl_no = 0;
        Log.e("SL No : ",String.valueOf(sl_no));
        Cursor cursorPersonalInfo = databaseHandler.rawQuery(chngeUpdQry);
        if(cursorPersonalInfo!=null){
            if(cursorPersonalInfo.moveToFirst()){
                do{

                    sl_no++;

                    Log.e("SL No : ",String.valueOf(sl_no));

                    String first_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("first_name"));
                    etFirstName.setText(first_name);

                    String last_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("last_name"));
                    etLastName.setText(last_name);

                    String name = first_name+" "+last_name;
                    tvFirst.setText(name);
//                  String id = String.valueOf(cursorPersonalInfo.getInt(cursorPersonalInfo.getColumnIndex("id")));

                    String id = String.valueOf(cursorPersonalInfo.getInt(0));
                    TempSaveData.PersonalInformationID = id;

                    String pic_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pic_name"));
                    Picasso.with(UdmtMemberViewDetailsActivity.this)
                            .load(ApiUtils.IMAGE_BASE_URL+pic_name)
                            .into(ivPhotoUpload);

                    String dob = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("dob"));

                    String doj = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("doj"));
                    etDateOfJoin.setText(dateFormatChange(doj));
                    String pin = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("pin"));
                    etPin.setText(pin);
                    String program_name = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("program_name"));
                    etProgramme.setText(program_name);
                    String committee_designation = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("committee_designation"));
                    etCommitteeDesignation.setText(committee_designation);
                    String company_designation = cursorPersonalInfo.getString(cursorPersonalInfo
                            .getColumnIndex("company_designation"));
                    etDesignation.setText(company_designation);
                    String phone = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("phone"));
                    String mobile = cursorPersonalInfo.getString(cursorPersonalInfo.getColumnIndex("mobile"));
                    etMobile.setText(mobile);


                }while(cursorPersonalInfo.moveToNext());
            }

        }
    }catch (Exception ex){}

//*********************** Select ICS Memeber Whoose Member Type Is ICS *****************************
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UdmtMemberViewDetailsActivity.this,
                        MemberProfileUpdateWithPhotoActivity.class);
                intent.putExtra("FROM_UDMT_VIEW","UdmtMemberViewDetailsActivity");
                TempSaveData.FROM = "UdmtMemberViewDetailsActivity";
                startActivity(intent);
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UdmtMemberViewDetailsActivity.this,
                        ListOfUdmtMemberAddNewActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UdmtMemberViewDetailsActivity.this,
                        ListOfUdmtMemberAddNewActivity.class);
                startActivity(intent);
                finish();
            }
        });
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UdmtMemberViewDetailsActivity.this,
                        SearchActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }//End Oncreate Method

    public String dateFormatChange(String date){

        //String ds1 = "2007-06-30";
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        String ds2 = null;
        try {
            ds2 = sdf2.format(sdf1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(ds2); //will be 30/06/2007

        return ds2;
    }


    //sharedPreferance....................................................................................
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            Intent idd = new Intent(UdmtMemberViewDetailsActivity.this, ListOfUdmtMemberAddNewActivity.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
