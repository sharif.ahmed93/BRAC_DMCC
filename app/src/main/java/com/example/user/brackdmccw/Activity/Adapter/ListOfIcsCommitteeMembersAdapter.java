package com.example.user.brackdmccw.Activity.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.user.brackdmccw.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 12/10/2017.
 */

public class ListOfIcsCommitteeMembersAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,String>> itemListContent = new ArrayList<>();
    HashMap<String, String> mapSingleContent = new HashMap<String, String>();

    ArrayList<String>spCommitteeDesignList = new ArrayList<>();

    public ListOfIcsCommitteeMembersAdapter(Context context,ArrayList<HashMap<String,
            String>>itemListContent){
        this.context = context;
        this.itemListContent = itemListContent;
    }

    @Override
    public int getCount() {
        return itemListContent.size();
    }

    @Override
    public Object getItem(int position) {
        return itemListContent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.ics_committe_list_row, null);
        final TextView tvSlNo = (TextView)view2.findViewById(R.id.tvSlNo);
        final TextView tvName = (TextView)view2.findViewById(R.id.tvName);
        final TextView tvPin = (TextView)view2.findViewById(R.id.tvPin);
        //final Spinner  spCommitteeDesignation = (Spinner)view2.findViewById(R.id
         //       .spCommitteeDesignation);
        final TextView tvCMTYDesignation = (TextView)view2.findViewById(R.id.tvCMTYDesignation);
        final TextView tvProgramme = (TextView)view2.findViewById(R.id.tvProgramme);
        final TextView tvDesignation = (TextView)view2.findViewById(R.id.tvDesignation);
        final TextView tvMobileNumber = (TextView)view2.findViewById(R.id.tvMobileNumber);

        mapSingleContent = itemListContent.get(position);
        tvSlNo.setText(mapSingleContent.get("sl_no"));
        tvName.setText(mapSingleContent.get("name"));
        tvPin.setText(mapSingleContent.get("pin"));
        tvProgramme.setText(mapSingleContent.get("programme"));
        tvDesignation.setText(mapSingleContent.get("company_designation"));
        tvMobileNumber.setText(mapSingleContent.get("mobile_number"));
        tvCMTYDesignation.setText(mapSingleContent.get("committee_designation"));

//        spCommitteeDesignList.clear();
//        spCommitteeDesignList.add("ICP");
//
//        ArrayAdapter<String> districtListAdapter = new ArrayAdapter<String>(context,R.layout
//                .spinner_text,spCommitteeDesignList);
//        districtListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCommitteeDesignation.setAdapter(districtListAdapter);



        return view2;
    }
}
