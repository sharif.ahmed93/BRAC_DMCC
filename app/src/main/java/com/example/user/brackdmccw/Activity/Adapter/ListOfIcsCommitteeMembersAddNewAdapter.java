package com.example.user.brackdmccw.Activity.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Activity.IcsMemberViewDetailsActivity;
import com.example.user.brackdmccw.Activity.Activity.ListOfIcsCommitteeMembersAddNewActivity;
import com.example.user.brackdmccw.Activity.Activity.ListOfUdmtMemberAddNewActivity;
import com.example.user.brackdmccw.Activity.Activity.LoginActivity;
import com.example.user.brackdmccw.Activity.Activity.MemberProfileUpdateWithPhotoActivity;
import com.example.user.brackdmccw.Activity.Activity.SearchActivity;
import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.NetworkCall.APIUtils.ApiUtils;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.MemberDeleteApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.UserLoginSuccessApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.MemberDeletePojo.MemberDeleteResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.CommitteeDesignation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.District;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.IcsUdmt;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.PersonalInformation;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.Upazilla;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.UserLoginSuccessResponse;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static junit.runner.BaseTestRunner.getPreference;

/**
 * Created by User on 12/10/2017.
 */

public class ListOfIcsCommitteeMembersAddNewAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,String>> itemListContent = new ArrayList<>();
    HashMap<String, String> mapSingleContent = new HashMap<String, String>();

    ArrayList<District>districtList = new ArrayList<District>();
    ArrayList<Upazilla>upazillaList = new ArrayList<Upazilla>();
    ArrayList<PersonalInformation>personalInformationList = new ArrayList<PersonalInformation>();
    ArrayList<IcsUdmt>icsUDMTList = new ArrayList<IcsUdmt>();
    ArrayList<CommitteeDesignation>committeeDesignationList = new ArrayList<CommitteeDesignation>();

    private HashMap<String,String> personalInfromationMap = new HashMap<>();
    private HashMap<String,String> icsUDMTMap = new HashMap<>();

    ArrayList<String>spCommitteeDesignList = new ArrayList<>();
    MemberDeleteApiService memberDeleteApiService;
    UserLoginSuccessApiService userLoginSuccessApiService;
    UserLoginSuccessResponse userLoginSuccessResponse;

    DatabaseHandler databaseHandler;

    String member_type_id = "";
    String committee_type_id="";

    public ListOfIcsCommitteeMembersAddNewAdapter(Context context,ArrayList<HashMap<String,
            String>>itemListContent){
        this.context = context;
        this.itemListContent = itemListContent;
        databaseHandler = new DatabaseHandler(context);
        memberDeleteApiService = ApiUtils.getMemberDeleteApiService();
        userLoginSuccessApiService = ApiUtils.getLoginSuccessAPIService();
    }

    @Override
    public int getCount() {
        return itemListContent.size();
    }

    @Override
    public Object getItem(int position) {
        return itemListContent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        final View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_list_of_ics_committee_row, null);

        //final TextView tvView = (TextView)view2.findViewById(R.id.tvView);
        //final TextView tvDelete = (TextView)view2.findViewById(R.id.tvDelete);

        final ImageView ivView = (ImageView)view2.findViewById(R.id.ivView);
        final ImageView ivDelete = (ImageView)view2.findViewById(R.id.ivDelete);

        final TextView tvSlNo = (TextView)view2.findViewById(R.id.tvSlNo);
        final TextView tvName = (TextView)view2.findViewById(R.id.tvName);
        final TextView tvPin = (TextView)view2.findViewById(R.id.tvPin);
        final TextView tvCommitteeDesignation = (TextView)view2.findViewById(R.id
                .tvCommitteeDesignation);
        final TextView tvProgramme = (TextView)view2.findViewById(R.id.tvProgramme);
        final TextView tvDesignation = (TextView)view2.findViewById(R.id.tvDesignation);
        final TextView tvMobileNumber = (TextView)view2.findViewById(R.id.tvMobileNumber);

        mapSingleContent = itemListContent.get(position);
        tvSlNo.setText(mapSingleContent.get("sl_no"));
        tvName.setText(mapSingleContent.get("name"));
        tvPin.setText(mapSingleContent.get("pin"));
        tvProgramme.setText(mapSingleContent.get("programme"));
        tvDesignation.setText(mapSingleContent.get("company_designation"));
        tvCommitteeDesignation.setText(mapSingleContent.get("committee_designation"));
        tvMobileNumber.setText(mapSingleContent.get("mobile_number"));

//        spCommitteeDesignList.clear();
//        spCommitteeDesignList.add("ICP");
//
//        ArrayAdapter<String> districtListAdapter = new ArrayAdapter<String>(context,R.layout.spinner_text,spCommitteeDesignList);
//        districtListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCommitteeDesignation.setAdapter(districtListAdapter);

        ivView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "Select View Position "+position, Toast.LENGTH_SHORT).show();
                TempSaveData.SelectIcsMemberViewPosition = itemListContent.get(position).get
                        ("pi_id");
                Log.e("Selected Member Position : ",itemListContent.get(position).get("pi_id"));
                Intent intent = new Intent(context, IcsMemberViewDetailsActivity.class);
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TempSaveData.SelectIcsMemberViewPosition = itemListContent.get(position).get("pi_id");
                Log.e("Selected Member Position : ",itemListContent.get(position).get("pi_id"));
//                Toast.makeText(context, "Select Delete Position "+position, Toast.LENGTH_SHORT)
//                        .show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure,You wanted to delete this ?");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {

                                        if(isNetworkConnected()){
                                            //*************Check Admin Or Member By Login Pin*************
                                            //String member_type_id = "";
//                                            String checkAdminQry = "SELECT dmcc_ics_udmt" +
//                                                    ".member_type_id,dmcc_ics_udmt.committee_type_id FROM " +
//                                                    "dmcc_personal_information " +
//                                                    "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
//                                                    "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";
//                                            Cursor adminCheckCursor = databaseHandler.rawQuery(checkAdminQry);

                                            //*************Check Admin Or Member By Login Pin*************
                                            String admin_or_member_by_pin = "";
                                            String admin_or_member_by_id = "";
                                            String committee_by_pin = "";
                                            String committee_by_id = "";

                                            String checkAdminQryPin = "SELECT dmcc_ics_udmt" +
                                                    ".member_type_id,dmcc_ics_udmt" +
                                                    ".committee_type_id FROM dmcc_personal_information " +
                                                    "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                                                    "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";

                                            String checkMemberQryId = "SELECT dmcc_ics_udmt.member_type_id,dmcc_ics_udmt" +
                                                    ".committee_type_id " +
                                                    "FROM dmcc_personal_information " +
                                                    "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                                                    "WHERE dmcc_personal_information.id = " +
                                                    ""+"'"+TempSaveData.SelectIcsMemberViewPosition+"' ";

                                            Cursor adminCheckPinCursor = databaseHandler.rawQuery
                                                    (checkAdminQryPin);
                                            Cursor memberCheckIdCursor = databaseHandler.rawQuery
                                                    (checkMemberQryId);

                                            if(adminCheckPinCursor != null){
                                                if(adminCheckPinCursor.moveToFirst()){
                                                    do{
                                                        admin_or_member_by_pin = String.valueOf
                                                                (adminCheckPinCursor.getInt(adminCheckPinCursor.getColumnIndex("member_type_id")));
                                                        committee_by_pin = String.valueOf
                                                                (adminCheckPinCursor.getInt(adminCheckPinCursor.getColumnIndex("committee_type_id")));
                                                        //TempSaveData.MemberTypeId = member_type_id;
                                                    }while (adminCheckPinCursor.moveToNext());
                                                }
                                            }
                                            if(memberCheckIdCursor != null){
                                                if(memberCheckIdCursor.moveToFirst()){
                                                    do{
                                                        Log.e("COUNT BY ID : ", String.valueOf(memberCheckIdCursor
                                                                .getCount()));
                                                        admin_or_member_by_id = String.valueOf
                                                                (memberCheckIdCursor.getInt(memberCheckIdCursor.getColumnIndex("member_type_id")));
                                                        committee_by_id = String.valueOf
                                                                (memberCheckIdCursor.getInt(memberCheckIdCursor.getColumnIndex("committee_type_id")));
                                                        //TempSaveData.MemberTypeId = member_type_id;
                                                    }while (memberCheckIdCursor.moveToNext());
                                                }
                                            }
                                            //*************End Check Admin Or Member By Login Pin*************
//
//
//                                            if(adminCheckCursor != null){
//                                                if(adminCheckCursor.moveToFirst()){
//                                                    do{
//                                                        member_type_id = String.valueOf(adminCheckCursor.getInt
//                                                                (adminCheckCursor.getColumnIndex("member_type_id")));
//                                                        committee_type_id = String.valueOf(adminCheckCursor.getInt
//                                                                (adminCheckCursor.getColumnIndex("committee_type_id")));
//                                                        TempSaveData.MemberTypeId = member_type_id;
//                                                    }while (adminCheckCursor.moveToNext());
//                                                }
//                                            }
//                                            //*************End Check Admin Or Member By Login Pin*************

//                                            if(member_type_id.equals("1")&& committee_type_id
//                                                    .equals("1")){
//                                                Toast.makeText(context, "Admin not deleted", Toast
//                                                        .LENGTH_SHORT).show();
//                                            }else if(member_type_id.equals("1")
//                                                    && committee_type_id.equals("2")){
//                                                Toast.makeText(context, "Udmt Admin Not Delete " +
//                                                        "Ics " +
//                                                        "member", Toast
//                                                        .LENGTH_SHORT).show();
//
//                                            }
                                            if(admin_or_member_by_pin.equals("1")
                                                    && admin_or_member_by_id.equals("1")
                                                    && committee_by_pin.equals("1")
                                                    && committee_by_id.equals("1")){

                                                Toast.makeText(context, "Admin Not Delete", Toast
                                                        .LENGTH_SHORT).show();
                                            }else if(admin_or_member_by_pin.equals("1")&& committee_by_pin.equals("1")){

                                                JSONObject paramObject = new JSONObject();
                                                try {
                                                    paramObject.put("id",TempSaveData.SelectIcsMemberViewPosition);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                Log.e("Login Member Type Id : ",admin_or_member_by_pin);
                                                Log.e("Login Committee Type Id : ",committee_by_pin);
                                                Log.e("Selected Member Position : ",itemListContent.get(position).get("pi_id"));
                                                Log.e("Selected Member Type Id: ",admin_or_member_by_id);
                                                Log.e("Selected Member Committee Type Id: ",committee_by_id);
                                                Log.e("Request JSON : ",paramObject.toString());

                                                memberDeleteApiService.requestForMemberDelete
                                                        (paramObject.toString())
                                                        .enqueue(new Callback<MemberDeleteResponse>() {
                                                            @Override
                                                            public void onResponse(Call<MemberDeleteResponse> call, Response<MemberDeleteResponse> response) {
                                                                if(response.isSuccessful()){

                                                                    MemberDeleteResponse memberDeleteResponse = response.body();
                                                                    Toast.makeText(context,"Status : "+memberDeleteResponse.getStatus(),Toast.LENGTH_SHORT).show();
                                                                    Log.e("Response Success Message : ","Response Is Successful");
                                                                    Log.e("Response Message : ", memberDeleteResponse.getMessage());
                                                                    Log.e("Response Status : ", memberDeleteResponse.getStatus());

                                                                    if(memberDeleteResponse
                                                                            .getStatus().equals
                                                                                    ("success")){

                                                                        String deleteIcsUdmtPIid =
                                                                                "DELETE FROM " +
                                                                                        "dmcc_ics_udmt " +
                                                                                        "WHERE " +
                                                                                        "personal_information_id " +
                                                                                        "= "+TempSaveData.SelectIcsMemberViewPosition;

                                                                        databaseHandler.rawQuery(deleteIcsUdmtPIid);
                                                                        databaseHandler.deleteTableRow("dmcc_personal_information",TempSaveData.SelectIcsMemberViewPosition);

                                                                        itemListContent.remove(position);
                                                                        notifyDataSetChanged();

                                                                        Toast.makeText(context,
                                                                                memberDeleteResponse.getMessage(), Toast
                                                                                        .LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<MemberDeleteResponse> call, Throwable t) {

                                                                Toast.makeText(context, "Failed", Toast
                                                                        .LENGTH_SHORT).show();
                                                                Log.e("ON FAIL : ",t.getMessage());

                                                            }
                                                        });
                                            }
                                            else{
                                                Toast.makeText(context, "Only Ics Admin Can " +
                                                        "Delete Memeber", Toast
                                                        .LENGTH_SHORT).show();
                                            }



                                        }else{

                                            Toast.makeText(context, "Please Check Your Network " +
                                                            "Connection",
                                                    Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        return view2;
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context
                .CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }

}
