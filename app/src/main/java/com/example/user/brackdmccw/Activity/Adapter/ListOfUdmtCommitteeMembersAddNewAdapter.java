package com.example.user.brackdmccw.Activity.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.brackdmccw.Activity.Activity.IcsMemberViewDetailsActivity;
import com.example.user.brackdmccw.Activity.Activity.UdmtMemberViewDetailsActivity;
import com.example.user.brackdmccw.Activity.Database.DatabaseHandler;
import com.example.user.brackdmccw.Activity.NetworkCall.APIUtils.ApiUtils;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.MemberDeleteApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.MemberDeletePojo.MemberDeleteResponse;
import com.example.user.brackdmccw.Activity.TempData.TempSaveData;
import com.example.user.brackdmccw.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 12/23/2017.
 */

public class ListOfUdmtCommitteeMembersAddNewAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,String>> itemListUdmtContent = new ArrayList<>();
    HashMap<String, String> mapUdmtSingleContent = new HashMap<String, String>();
    MemberDeleteApiService memberDeleteApiService;
    DatabaseHandler databaseHandler;
    public ListOfUdmtCommitteeMembersAddNewAdapter(Context context,ArrayList<HashMap<String,
            String>>itemListContent){
        this.context = context;
        this.itemListUdmtContent = itemListContent;
        databaseHandler = new DatabaseHandler(context);
        memberDeleteApiService = ApiUtils.getMemberDeleteApiService();
    }

    ArrayList<String>spCommitteeDesignList = new ArrayList<>();
    @Override
    public int getCount() {
        return itemListUdmtContent.size();
    }

    @Override
    public Object getItem(int position) {
        return itemListUdmtContent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_list_of_ics_committee_row, null);

        //final TextView tvView = (TextView)view2.findViewById(R.id.tvView);
        //final TextView tvDelete = (TextView)view2.findViewById(R.id.tvDelete);

        final ImageView ivView = (ImageView)view2.findViewById(R.id.ivView);
        final ImageView ivDelete = (ImageView)view2.findViewById(R.id.ivDelete);

        final TextView tvSlNo = (TextView)view2.findViewById(R.id.tvSlNo);
        final TextView tvName = (TextView)view2.findViewById(R.id.tvName);
        final TextView tvPin = (TextView)view2.findViewById(R.id.tvPin);
        final TextView tvCommitteeDesignation = (TextView)view2.findViewById(R.id
                .tvCommitteeDesignation);
        final TextView tvProgramme = (TextView)view2.findViewById(R.id.tvProgramme);
        final TextView tvDesignation = (TextView)view2.findViewById(R.id.tvDesignation);
        final TextView tvMobileNumber = (TextView)view2.findViewById(R.id.tvMobileNumber);

        mapUdmtSingleContent = itemListUdmtContent.get(position);
        tvSlNo.setText(mapUdmtSingleContent.get("sl_no"));
        tvName.setText(mapUdmtSingleContent.get("name"));
        tvPin.setText(mapUdmtSingleContent.get("pin"));
        tvProgramme.setText(mapUdmtSingleContent.get("programme"));
        tvDesignation.setText(mapUdmtSingleContent.get("company_designation"));
        tvMobileNumber.setText(mapUdmtSingleContent.get("mobile_number"));
        tvCommitteeDesignation.setText(mapUdmtSingleContent.get("committee_designation"));

//        spCommitteeDesignList.clear();
//        spCommitteeDesignList.add("ICP");
//
//        ArrayAdapter<String> districtListAdapter = new ArrayAdapter<String>(context,R.layout.spinner_text,spCommitteeDesignList);
//        districtListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCommitteeDesignation.setAdapter(districtListAdapter);

        ivView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(context, "Select View Position "+itemListUdmtContent.get(position).get("pi_id"), Toast.LENGTH_SHORT).show();
                TempSaveData.SelectUDMTMemberViewPosition = itemListUdmtContent.get(position).get("pi_id");
                Log.e("Selected Member Position : ",itemListUdmtContent.get(position).get("pi_id"));
                Intent intent = new Intent(context, UdmtMemberViewDetailsActivity.class);
                context.startActivity(intent);
                ((Activity)context).finish();


            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TempSaveData.SelectUDMTMemberViewPosition = itemListUdmtContent.get(position).get("pi_id");
                Log.e("Selected Member Position : ",itemListUdmtContent.get(position).get("pi_id"));



                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure,You wanted to delete this ?");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if(isNetworkConnected()){
                                    //*************Check Admin Or Member By Login Pin*************
                                    String admin_or_member_by_pin = "";
                                    String admin_or_member_by_id = "";
                                    String committee_by_pin = "";
                                    String committee_by_id = "";

                                    String checkAdminQryPin = "SELECT dmcc_ics_udmt" +
                                            ".member_type_id,dmcc_ics_udmt" +
                                            ".committee_type_id FROM dmcc_personal_information " +
                                            "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                                            "WHERE dmcc_personal_information.pin = "+"'"+getPreference("pin")+"' ";

                                    String checkMemberQryId = "SELECT dmcc_ics_udmt.member_type_id,dmcc_ics_udmt" +
                                            ".committee_type_id " +
                                            "FROM dmcc_personal_information " +
                                            "LEFT JOIN dmcc_ics_udmt ON dmcc_personal_information.id = dmcc_ics_udmt.personal_information_id " +
                                            "WHERE dmcc_personal_information.id = " +
                                            ""+"'"+TempSaveData.SelectUDMTMemberViewPosition+"' ";

                                    Cursor adminCheckPinCursor = databaseHandler.rawQuery
                                            (checkAdminQryPin);
                                    Cursor memberCheckIdCursor = databaseHandler.rawQuery
                                            (checkMemberQryId);

                                    if(adminCheckPinCursor != null){
                                        if(adminCheckPinCursor.moveToFirst()){
                                            do{
                                                admin_or_member_by_pin = String.valueOf
                                                        (adminCheckPinCursor.getInt(adminCheckPinCursor.getColumnIndex("member_type_id")));
                                                committee_by_pin = String.valueOf
                                                        (adminCheckPinCursor.getInt(adminCheckPinCursor.getColumnIndex("committee_type_id")));
                                                //TempSaveData.MemberTypeId = member_type_id;
                                            }while (adminCheckPinCursor.moveToNext());
                                        }
                                    }
                                    if(memberCheckIdCursor != null){
                                        if(memberCheckIdCursor.moveToFirst()){
                                            do{
                                                Log.e("COUNT BY ID : ", String.valueOf(memberCheckIdCursor
                                                        .getCount()));
                                                admin_or_member_by_id = String.valueOf
                                                        (memberCheckIdCursor.getInt(memberCheckIdCursor.getColumnIndex("member_type_id")));
                                                committee_by_id = String.valueOf
                                                        (memberCheckIdCursor.getInt(memberCheckIdCursor.getColumnIndex("committee_type_id")));
                                                //TempSaveData.MemberTypeId = member_type_id;
                                            }while (memberCheckIdCursor.moveToNext());
                                        }
                                    }
                                    //*************End Check Admin Or Member By Login Pin*************
                                    if(admin_or_member_by_pin.equals("1")
                                            && admin_or_member_by_id.equals("1")
                                            && committee_by_pin.equals("2")
                                            && committee_by_id.equals("2")){

                                        Toast.makeText(context, "Admin Not Delete", Toast
                                                .LENGTH_SHORT).show();
                                    }else{
                                        JSONObject paramObject = new JSONObject();
                                        try {
                                            paramObject.put("id",TempSaveData
                                                    .SelectUDMTMemberViewPosition);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                        Log.e("Login Member Type Id : ",admin_or_member_by_pin);
                                        Log.e("Login Committee Type Id : ",committee_by_pin);
                                        Log.e("Selected Member Position : ",itemListUdmtContent.get(position).get("pi_id"));
                                        Log.e("Selected Member Type Id: ",admin_or_member_by_id);
                                        Log.e("Selected Member Committee Type Id: ",committee_by_id);
                                        Log.e("Request JSON : ",paramObject.toString());

                                        memberDeleteApiService.requestForMemberDelete
                                                (paramObject.toString())
                                                .enqueue(new Callback<MemberDeleteResponse>() {
                                                    @Override
                                                    public void onResponse(Call<MemberDeleteResponse> call, Response<MemberDeleteResponse> response) {
                                                        if(response.isSuccessful()){

                                                            Log.e("Response Success Message : ","Response Is Successful");

                                                            MemberDeleteResponse memberDeleteResponse = response.body();
                                                            Toast.makeText(context,"Status : "+memberDeleteResponse.getStatus(),Toast.LENGTH_SHORT).show();
                                                            Log.e("Response Message : ", memberDeleteResponse.getMessage());
                                                            Log.e("Response Status : ", memberDeleteResponse.getStatus());
                                                            if(memberDeleteResponse.getStatus()
                                                                    .equals("success")){

                                                                String deleteIcsUdmtPIid =
                                                                        "DELETE FROM " +
                                                                                "dmcc_ics_udmt " +
                                                                                "WHERE " +
                                                                                "personal_information_id " +
                                                                                "= "+TempSaveData.SelectUDMTMemberViewPosition;

                                                                databaseHandler.rawQuery(deleteIcsUdmtPIid);
                                                                databaseHandler.deleteTableRow("dmcc_personal_information",TempSaveData.SelectUDMTMemberViewPosition);

                                                                itemListUdmtContent.remove(position);
                                                                notifyDataSetChanged();

                                                                Toast.makeText(context,
                                                                        memberDeleteResponse.getMessage(), Toast
                                                                                .LENGTH_SHORT).show();
                                                            }
                                                        }else{
                                                            Log.e("Response Success Message : ",
                                                                    "Response Is Not Successful");
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<MemberDeleteResponse> call, Throwable t) {

                                                        Toast.makeText(context, "Failed", Toast
                                                                .LENGTH_SHORT).show();
                                                        Log.e("ON FAIL : ",t.getMessage());

                                                    }
                                                });

                                    }

                                }else{

                                    Toast.makeText(context, "Please Check Your Network " +
                                                    "Connection",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        return view2;
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context
                .CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }
}
