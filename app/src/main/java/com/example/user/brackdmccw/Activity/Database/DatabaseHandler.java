package com.example.user.brackdmccw.Activity.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "brac_dmcc";


	private static final String TABLE_DMCC_PERSONAL_INFORMATION="dmcc_personal_information";

	private static final String TABLE_DISTRICT="district";
	private static final String TABLE_UPAZILLA="upazilla";
	private static final String TABLE_DIVISION="division";

	private static final String TABLE_DMCC_ICS_UDMT="dmcc_ics_udmt";
	private static final String TABLE_DMCC_COMMITTEE_DESIGNATION="dmcc_committee_designation";
	private static final String TABLE_MEMBER_TYPE="dmcc_member_type";
	private static final String TABLE_COMMITTEE_TYPE="dmcc_committee_type";





	public DatabaseHandler(Context context) {
		
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
	
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {



		String CREATE_TABLE_DMCC_PERSONAL_INFORMATION = "CREATE TABLE IF NOT EXISTS " +
				TABLE_DMCC_PERSONAL_INFORMATION +
				"("
				+ "id" + " INTEGER PRIMARY KEY," +
				"first_name" + " TEXT,"	+
				"last_name" + " TEXT,"	+
				"pic_name" + " TEXT,"	+
				"dob" + " TEXT,"	+
				"doj" + " TEXT,"	+
				"pin" + " TEXT,"	+
				"program_name" + " TEXT,"	+
				"company_designation" + " TEXT,"	+
				"phone" + " TEXT,"	+
				"mobile" + " TEXT,"	+
				"current_work_station" + " TEXT,"	+
				"created_by" + " INTEGER,"	+
				"updated_by" + " INTEGER,"	+
				"created_at" + " datetime,"	+
				"updated_at" + " TIMESTAMP" +
				")";
		db.execSQL(CREATE_TABLE_DMCC_PERSONAL_INFORMATION);

		String CREATE_DIVISIONS = "CREATE TABLE IF NOT EXISTS " + TABLE_DIVISION + "("
				+ "id" + " INTEGER PRIMARY KEY," +
				"division_name" + " TEXT,"	+
				"updated_at" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_DIVISIONS);

		String CREATE_DISTRICTS = "CREATE TABLE IF NOT EXISTS " + TABLE_DISTRICT + "("
				+ "id" + " INTEGER PRIMARY KEY," +
				"division_id" + " INTEGER,"	+
				"district_name" + " TEXT,"	+
				"updated_at" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_DISTRICTS);

		String CREATE_UPAZILLA = "CREATE TABLE IF NOT EXISTS " + TABLE_UPAZILLA + "("
				+ "id" + " INTEGER PRIMARY KEY," +
				"district_id" + " INTEGER,"	+
				"upazilla_name" + " TEXT,"	+
				"updated_at" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_UPAZILLA);



		String CREATE_TABLE_DMCC_ICS_UDMT = "CREATE TABLE IF NOT EXISTS " +
				TABLE_DMCC_ICS_UDMT +
				"("
				+ "id" + " INTEGER PRIMARY KEY," +
				"personal_information_id" + " INTEGER,"	+
				"division_id" + " INTEGER,"	+
				"district_id" + " INTEGER,"	+
				"upazila_id" + " INTEGER,"	+
				"committee_type_id" + " INTEGER,"	+
				"member_type_id" + " INTEGER,"	+
				"committee_designation_id" + " INTEGER,"	+
				"status" + " TEXT,"	+
				"created_by" + " INTEGER,"	+
				"updated_by" + " INTEGER,"	+
				"created_at" + " datetime,"	+
				"updated_at" + " timestamp,"+
				"updated_at_mob" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_TABLE_DMCC_ICS_UDMT);

		String CREATE_MEMBER_TYPE = "CREATE TABLE IF NOT EXISTS " + TABLE_MEMBER_TYPE + "("
				+ "id" + " INTEGER PRIMARY KEY," +
				"member_type" + " TEXT,"	+
				"created_by" + " INTEGER,"	+
				"created_at" + " datetime,"	+
				"updated_by" + " INTEGER,"	+
				"updated_at" + " datetime,"+
				"updated_at_mob" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_MEMBER_TYPE);

		String CREATE_COMMITTEE_TYPE = "CREATE TABLE IF NOT EXISTS " + TABLE_COMMITTEE_TYPE + "("
				+ "id" + " INTEGER PRIMARY KEY," +
				"committee_type" + " TEXT,"	+
				"created_by" + " INTEGER,"	+
				"created_at" + " datetime,"	+
				"updated_by" + " INTEGER,"	+
				"updated_at" + " datetime,"+
				"updated_at_mob" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_COMMITTEE_TYPE);

		String CREATE_DMCC_COMMITTEE_DESIGNATION = "CREATE TABLE IF NOT EXISTS " +
				TABLE_DMCC_COMMITTEE_DESIGNATION + "("
				+ "id" + " INTEGER PRIMARY KEY," +
				"committee_designation" + " TEXT,"	+
				"created_by" + " INTEGER,"	+
				"created_at" + " datetime,"	+
				"updated_by" + " INTEGER,"	+
				"updated_at" + " timestamp,"+
				"updated_at_mob" + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))" +
				")";
		db.execSQL(CREATE_DMCC_COMMITTEE_DESIGNATION);


	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		onCreate(db);
	}


	public void deleteTableRow(String tableName, String row)
	{
		SQLiteDatabase db1 = this.getReadableDatabase();
		db1.execSQL("DELETE  FROM " + tableName+" where id='"+row+"'");

		Log.e("DELETED:",tableName);
	}

	public long InsertTable(HashMap<String, String> data, String TableName) {

		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues values = new ContentValues();

		Iterator it = data.entrySet().iterator();
		long insertedID = 0;

		while(it.hasNext())
		{
			Entry<String, String> pair = (Entry)it.next();
			values.put(pair.getKey(), pair.getValue()); // Contact Name
			
			it.remove();
		}

		Log.e(" Content values  :",values.toString());
		Log.e(TableName,"   :"+"INSERTED");
		insertedID = db.insert(TableName, null, values);
		db.close();
		return insertedID;

	}

    public void UpdateTable(HashMap<String,String> map, String tableName, String updateCullumn, String UpdateCullumnValue)
    {
        String Query="UPDATE "+tableName+" SET ";
        for (Entry<String, String> entry : map.entrySet()) {
            Query=Query+entry.getKey()+"= '"+entry.getValue()+"',";
        }
        Query=Query.substring(0,Query.length()-1)+" WHERE "+updateCullumn+"= "+UpdateCullumnValue;
        Log.e("Query", Query);
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(Query);

    }


    public boolean checkDuplicate(String tableName, String culumnName, String culumnValue)
    {
        String getDate = "";
        SQLiteDatabase db = this.getReadableDatabase();
        if(tableName.equalsIgnoreCase(tableName))
            Log.e("Duplicate Check Query","SELECT * FROM "+tableName+" WHERE "+culumnName+"= '"+culumnValue+"'");

        Cursor c = db.rawQuery("SELECT * FROM "+tableName+" WHERE "+culumnName+"= '"+culumnValue+"'", null);
        if(c.getCount()>0)
            return true;
        else
            return false;
    }

	public void excQuery(String query) {
		SQLiteDatabase db = this.getReadableDatabase();
		 db.execSQL(query);

	}


	
	public Cursor rawQuery(String query) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(query, null);
//		db.close();
		return c;
	}


	public void dropTable(String table_name)
	{

		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("DROP TABLE IF EXISTS '" + table_name + "'");
	}

	public void deleteAllRow(String tableName)
	{
		SQLiteDatabase db1 = this.getReadableDatabase();
		db1.execSQL("DELETE FROM " + tableName);

		Log.e("DELETED:",tableName);
	}
}