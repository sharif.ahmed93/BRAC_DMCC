package com.example.user.brackdmccw.Activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;




import android.util.Log;

public class GetJson {
	
	public static String fetchJSON(String url, ArrayList<String> key, ArrayList<String> values) {
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		httppost.setHeader("Accept", "application/json");
		httppost.setHeader("Content-type", "application/json");
		
		Log.e("Url", url);
			
		try
		{
			JSONObject obj = new JSONObject();	
			for(int i=0;i<key.size();i++)
			{
				obj.put(key.get(i), values.get(i));
				Log.e("Parameter", key.get(i)+":"+values.get(i));
			}
		   
			Log.e("Json", obj.toString());
			StringEntity entity1 = new StringEntity(obj.toString(), HTTP.UTF_8);
			entity1.setContentType("application/json");
			httppost.setEntity(entity1);
			HttpResponse response = httpclient.execute(httppost);
			 

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				return EntityUtils.toString(entity);
			}
		}catch(Exception e){}
		
		return "NO RESPONSE";
	}
}
