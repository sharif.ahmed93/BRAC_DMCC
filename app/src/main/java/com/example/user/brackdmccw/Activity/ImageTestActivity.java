package com.example.user.brackdmccw.Activity;

import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.user.brackdmccw.Activity.Activity.MemberProfileUpdateWithPhotoActivity;
import com.example.user.brackdmccw.R;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageTestActivity extends AppCompatActivity {

    Button btnImagePickGalary;
    Button btnImagePickCamera;
    ImageView ivSet;
    Button btnBase64;
    ImageView ivBase64Set;

    private static int GALLAY_PICTURE_REQUEST = 1;
    private static final int CAMERA_PICTURE_REQUEST = 2;
    Bitmap actualBitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_test);

        btnImagePickGalary = (Button) findViewById(R.id.btnImagePickGalary);
        btnImagePickCamera = (Button) findViewById(R.id.btnImagePickCamera);
        btnBase64 = (Button) findViewById(R.id.btnBase64);
        ivSet = (ImageView) findViewById(R.id.ivSet);
        ivBase64Set = (ImageView) findViewById(R.id.ivBase64Set);

        btnImagePickGalary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePictureAction();
            }
        });
//        btnImagePickCamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                choosePictureAction();
//            }
//        });

        btnBase64.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imageString = imageToString(actualBitmap);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("photo","data:image/png;base64,"+imageString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("BASE64 StrIng : ",imageString);
                Log.e("BASE64 JSON : ",jsonObject.toString());
//                String imageStringPhotoUtil = ImageBase64.encode(actualBitmap);
//                Bitmap imageStringPhotoUtilBitmap = ImageBase64.decode(imageStringPhotoUtil);

                byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ivBase64Set.setImageBitmap(decodedByte);
            }
        });

    }

//**************************** Photo Result From Gallary or Camera ************************

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PICTURE_REQUEST && resultCode == RESULT_OK) {
            actualBitmap = (Bitmap) data.getExtras().get("data");
            ivSet.setImageBitmap(actualBitmap);
        }

        if (requestCode == GALLAY_PICTURE_REQUEST && resultCode == RESULT_OK) {
            try {
                actualBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(data.getData()));
                ivSet.setImageBitmap(actualBitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }//End OnActivityResult Method


    //**************************** Choose Picture Option From Dialogue ************************
    private void choosePictureAction() {

        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app
                .AlertDialog.Builder(ImageTestActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (items[which].equals("Camera")) {

//                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, CAMERA_PICTURE_REQUEST);
                    TakePhoto();
//                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(cameraIntent, CAMERA_REQUEST);


                } else if (items[which].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLAY_PICTURE_REQUEST);


                } else if (items[which].equals("Cancel")) {

                    dialog.dismiss();

                }

            }
        });

        builder.show();

    }
//**************************** End Choose Picture Option From Dialogue ************************
//**************************** End Photo Result From Gallary or Camera ************************


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    //Get Image Path Location From Uri
    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private String imageToString (Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgTobByteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgTobByteArray,Base64.DEFAULT);
    }
    public void TakePhoto()
    {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_PICTURE_REQUEST);
    }
}
