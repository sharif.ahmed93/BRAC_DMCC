package com.example.user.brackdmccw.Activity.NetworkCall.APIUtils;


import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.AddMemberAPIService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.AdminProfileUpdateApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.MemberDeleteApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.UserLoginAPIService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices.UserLoginSuccessApiService;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.Client.RetrofitClient;

/**
 * Created by User on 12/13/2017.
 */

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "http://192.168.0.7/dmcc_w/app/index.php/api/";//Arena
    public static final String IMAGE_BASE_URL="http://192.168.0.7/dmcc_w/app/upload_file/personal_image/";//Arena


//    public static final String BASE_URL = "http://172.26.5.167/dmcc_w/app/index.php/api/";//BRAC
//    public static final String IMAGE_BASE_URL ="http://172.26.5.167/dmcc_w/app/upload_file/personal_image/";//BRAC

    public static UserLoginAPIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(UserLoginAPIService.class);
    }

    public static UserLoginSuccessApiService getLoginSuccessAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(UserLoginSuccessApiService.class);
    }

    public static AdminProfileUpdateApiService getAdminProfileUpdateApiService() {

        return RetrofitClient.getClient(BASE_URL).create(AdminProfileUpdateApiService.class);
    }
    public static MemberDeleteApiService getMemberDeleteApiService() {

        return RetrofitClient.getClient(BASE_URL).create(MemberDeleteApiService.class);
    }

    public static AddMemberAPIService getAddMemberAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(AddMemberAPIService.class);
    }
}
