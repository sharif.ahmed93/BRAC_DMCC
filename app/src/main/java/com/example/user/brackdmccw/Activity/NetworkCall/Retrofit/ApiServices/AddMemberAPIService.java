package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices;

import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.AddMemberPojo.AddMemberResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by User on 1/1/2018.
 */

public interface AddMemberAPIService {
    @Headers("Content-Type: application/json")
    @POST("")//AddMember API EndPoint
    Call<AddMemberResponse> requestForAddMember(@Body String requestJson);
}
