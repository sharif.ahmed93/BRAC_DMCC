package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices;

import com.example.user.brackdmccw.Activity.Activity.MemberProfileUpdateWithPhotoActivity;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.ProfileUpdatePojo.ProfileUpdateResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.LoginResponse;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by User on 12/19/2017.
 */

public interface AdminProfileUpdateApiService {
    @Headers("Content-Type: application/json")
    @POST("set_profile_updated_data")
    //Call<LoginResponse> requestForAdminProfileUpdate(@Body HashMap<String,String>profileUpdateMap);
    Call<ProfileUpdateResponse> requestForProfileUpdate(@Body String profileUpdateData);



}
