package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices;

import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.MemberDeletePojo.MemberDeleteResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.ProfileUpdatePojo.ProfileUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by User on 12/26/2017.
 */

public interface MemberDeleteApiService {
    @Headers("Content-Type: application/json")
    @POST("delete_person_data")
    Call<MemberDeleteResponse> requestForMemberDelete(@Body String memberId);
}
