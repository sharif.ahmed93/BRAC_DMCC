package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices;

import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.LoginResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.User;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.UserLoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by User on 12/13/2017.
 */

public interface UserLoginAPIService {
    //@GET("user_pin_login")
    //@FormUrlEncoded
//    Call<UserLoginResponse> requestForLogin(
//           // @Field("pin") String pin,
//           // @Field("dob") String dob
//    );



    @Headers("Content-Type: application/json")
    @POST("user_pin_login")
    Call<LoginResponse> requestForLogin(@Body String body);

}
