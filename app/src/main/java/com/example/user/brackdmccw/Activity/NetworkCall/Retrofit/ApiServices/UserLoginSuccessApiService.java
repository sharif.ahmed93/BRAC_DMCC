package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.ApiServices;

import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginPojo.UserLoginResponse;
import com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo.UserLoginSuccessResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by User on 12/17/2017.
 */

public interface UserLoginSuccessApiService {
    @GET("get_required_data")
    Call<UserLoginSuccessResponse> requestForGetInformation();
}
