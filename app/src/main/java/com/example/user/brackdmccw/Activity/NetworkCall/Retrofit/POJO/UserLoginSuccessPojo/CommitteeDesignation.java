
package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommitteeDesignation {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("committee_designation")
    @Expose
    private String committeeDesignation;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommitteeDesignation() {
        return committeeDesignation;
    }

    public void setCommitteeDesignation(String committeeDesignation) {
        this.committeeDesignation = committeeDesignation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
