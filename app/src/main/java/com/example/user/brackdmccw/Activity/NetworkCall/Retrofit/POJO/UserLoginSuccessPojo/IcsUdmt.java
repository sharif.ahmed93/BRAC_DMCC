
package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IcsUdmt {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("personal_information_id")
    @Expose
    private String personalInformationId;
    @SerializedName("division_id")
    @Expose
    private String divisionId;
    @SerializedName("district_id")
    @Expose
    private String districtId;
    @SerializedName("upazila_id")
    @Expose
    private String upazilaId;
    @SerializedName("committe_type_id")
    @Expose
    private String committeTypeId;
    @SerializedName("member_type_id")
    @Expose
    private String memberTypeId;
    @SerializedName("committee_designation_id")
    @Expose
    private String committeeDesignationId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPersonalInformationId() {
        return personalInformationId;
    }

    public void setPersonalInformationId(String personalInformationId) {
        this.personalInformationId = personalInformationId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getUpazilaId() {
        return upazilaId;
    }

    public void setUpazilaId(String upazilaId) {
        this.upazilaId = upazilaId;
    }

    public String getCommitteTypeId() {
        return committeTypeId;
    }

    public void setCommitteTypeId(String committeTypeId) {
        this.committeTypeId = committeTypeId;
    }

    public String getMemberTypeId() {
        return memberTypeId;
    }

    public void setMemberTypeId(String memberTypeId) {
        this.memberTypeId = memberTypeId;
    }

    public String getCommitteeDesignationId() {
        return committeeDesignationId;
    }

    public void setCommitteeDesignationId(String committeeDesignationId) {
        this.committeeDesignationId = committeeDesignationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
