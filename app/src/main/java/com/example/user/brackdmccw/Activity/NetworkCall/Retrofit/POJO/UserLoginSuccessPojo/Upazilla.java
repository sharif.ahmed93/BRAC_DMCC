
package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Upazilla {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("district_id")
    @Expose
    private String districtId;
    @SerializedName("thana_name")
    @Expose
    private String thanaName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getThanaName() {
        return thanaName;
    }

    public void setThanaName(String thanaName) {
        this.thanaName = thanaName;
    }

}
