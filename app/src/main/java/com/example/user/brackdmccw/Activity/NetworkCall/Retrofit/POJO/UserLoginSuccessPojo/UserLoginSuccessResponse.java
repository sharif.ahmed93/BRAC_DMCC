
package com.example.user.brackdmccw.Activity.NetworkCall.Retrofit.POJO.UserLoginSuccessPojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginSuccessResponse {

    @SerializedName("personal_information")
    @Expose
    private List<PersonalInformation> personalInformation = null;
    @SerializedName("ics_udmt")
    @Expose
    private List<IcsUdmt> icsUdmt = null;
    @SerializedName("districts")
    @Expose
    private List<District> districts = null;
    @SerializedName("upazilla")
    @Expose
    private List<Upazilla> upazilla = null;
    @SerializedName("committee_designations")
    @Expose
    private List<CommitteeDesignation> committeeDesignations = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<PersonalInformation> getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(List<PersonalInformation> personalInformation) {
        this.personalInformation = personalInformation;
    }

    public List<IcsUdmt> getIcsUdmt() {
        return icsUdmt;
    }

    public void setIcsUdmt(List<IcsUdmt> icsUdmt) {
        this.icsUdmt = icsUdmt;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<Upazilla> getUpazilla() {
        return upazilla;
    }

    public void setUpazilla(List<Upazilla> upazilla) {
        this.upazilla = upazilla;
    }

    public List<CommitteeDesignation> getCommitteeDesignations() {
        return committeeDesignations;
    }

    public void setCommitteeDesignations(List<CommitteeDesignation> committeeDesignations) {
        this.committeeDesignations = committeeDesignations;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
